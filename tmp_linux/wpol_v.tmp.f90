












!===================================================================
!
! Macros for the complex pre-processing. All source files with extension
! .F90z are pre-processed twice. Once with CPLX and then again without CPLX.
! Most instances of capital "Z" are replaced by either lower case "z" or
! lower case "d" respectively with or without CPLX.
! BE CAREFUL WITH CAPITAL "Z" IN .F90z FILES !!! IT HAS SPECIAL MEANING!!!
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
! Constants
! Implicit functions
! External functions and subroutines
! Internal functions and subroutines
! Internal arrays
!===================================================================
!===================================================================
!
! Calculate wpol=X in real space and write it on scratch files.
! All the output of this subroutine is written to files TMPMATEL_*.
! Value of input parameters is not modified.
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
subroutine dwpol_v(gvec,kpt,pol,nolda,irp,nspin,nr_buff,itape,qcoord)

  use typedefs
  use mpi_module
  use xc_functionals
  use fft_module
  implicit none

  ! arguments
  ! real-space grid
  type (gspace), intent(inout) :: gvec
  ! k-points and electron wavefunctions (from DFT)
  type (kptinfo), intent(in) :: kpt
  ! TDDFT polarizability
  type (polinfo), intent(inout) :: pol
  ! true if LDA kernel is ignored
  logical, intent(in) :: nolda
  integer, intent(in) :: &
       irp, &         ! current representation
       nspin, &       ! number of spin channels
       nr_buff, &     ! length of output buffer (defines at how many
                      ! points the static limit is calculated)
       itape          ! output unit
  ! coordinates of current q-vector, in units of reciprocal lattice vectors
  real(dp) :: qcoord(3)

  ! local variables
  real(dp), parameter :: tol_l = 1.d-8
  type(xc_type) :: xc_lda
  integer :: ii, isp, jsp, info, ipol, ipe, m1, k1, m2, k2, &
       m2old, k2old, ispold, ngrid, ngrid_pe, ipol_pe, ioff
  real(dp) :: qkt(3), qkt_test(3)
  real(dp) :: xtmp
  real(dp) ,dimension(:), allocatable :: wfn1, wfn2, vr
  real(dp), dimension(:,:), allocatable :: fr, fr_distr
  real(dp), dimension(:,:,:), allocatable :: zxc
  real(dp), dimension(:,:,:), allocatable :: fxc

  ngrid_pe = w_grp%ldn * w_grp%npes
  ngrid = w_grp%mydim
  xtmp = one / gvec%hcub

  !-------------------------------------------------------------------
  ! Calculate coulomb interaction,
  ! V_coul = 4*pi*e^2/q^2 (rydberg units, e^2 = 2).
  !
  qkt = zero
  call dinitialize_FFT(peinf%inode,fft_box)
  if (gvec%per == 1) then
     call dcreate_coul_1D(peinf%inode,peinf%npes,peinf%comm,qcoord(1),gvec,fft_box)
  elseif (gvec%per == 2) then
     call dcreate_coul_2D(gvec%bdot,qcoord(1),fft_box)
  else
     call dcreate_coul_0D(gvec%bdot,qcoord,fft_box)
  endif

  !-------------------------------------------------------------------
  ! Initialize arrays.
  !
  allocate(wfn1(ngrid_pe),stat=info)
  call alccheck('wfn1','wpol_v',ngrid_pe,info)
  wfn1 = zero
  allocate(wfn2(ngrid),stat=info)
  call alccheck('wfn2','wpol_v',ngrid,info)

  allocate(vr(gvec%nr),stat=info)
  call alccheck('vr','wpol_v',gvec%nr,info)
  allocate(fr(gvec%nr,nspin),stat=info)
  call alccheck('fr','wpol_v',gvec%nr*nspin,info)
  allocate(fr_distr(ngrid_pe,nspin),stat=info)
  call alccheck('fr_distr','wpol_v',ngrid_pe*nspin,info)
  fr_distr = zero

  !-------------------------------------------------------------------
  ! Calculate LDA kernel.
  !
  allocate(zxc(ngrid,nspin,nspin),stat=info)
  call alccheck('zxc','wpol_v',ngrid*nspin*nspin,info)
  zxc = zero

  if (nolda) then
     if (r_grp%master) write(6,'(/,a,/,a,/,a,/)') repeat('=',65), &
          ' WARNING !!!! f_lda = zero ', repeat('=',65)
  else
     allocate(fxc(ngrid,nspin,nspin),stat=info)
     call alccheck('fxc','wpol_v',ngrid*nspin*nspin,info)
     fxc = zero
     do isp = 1, nspin
        do ii = 1, ngrid
           fxc(ii,isp,1) = kpt%rho(w_grp%offset+ii,isp)
        enddo
     enddo
     call xc_init(nspin,XC_LDA_X,XC_LDA_C_PZ,0,zero,one,.false.,xc_lda)
     xc_lda%has_grad = .false.
     call fxc_get(xc_lda,nspin,ngrid,1,fxc)
     zxc = fxc * one
     call xc_end(xc_lda)
     deallocate(fxc)
  endif

  !-------------------------------------------------------------------
  ! Start calculating vvc, fvc.
  !
  call stopwatch(r_grp%master, &
       ' Memory allocation done. Start calculating vvc,fvc.')
  m2old = 0
  k2old = 0
  ispold = 0
  do ipol_pe = 1, pol%ntr, r_grp%npes
     do ipe = 0, w_grp%npes - 1
        ipol = w_grp%mygr*w_grp%npes + ipol_pe + ipe
        if (ipol > pol%ntr) exit
        ioff = w_grp%ldn*ipe + 1
        m2 = pol%tr(2,ipol)
        k2 = pol%tr(4,ipol)
        isp = 1
        if (ipol > pol%n_up) isp = 2
        if (m2 /= m2old .or. isp /= ispold .or. k2 /= k2old) then
           ii = kpt%wfn(isp,k2)%map(m2)
           wfn2(1:ngrid) = kpt%wfn(isp,k2)%dwf(1:ngrid,ii)
           m2old = m2
           k2old = k2
           ispold = isp
        endif
        m1 = pol%tr(1,ipol)
        k1 = pol%tr(3,ipol)
        ii = kpt%wfn(isp,k1)%map(m1)
        wfn1(ioff:ioff + ngrid - 1) = (kpt%wfn(isp,k1)%dwf(1:ngrid,ii))
        call dmultiply_vec(ngrid,wfn2,wfn1(ioff))
        call dscal(ngrid,xtmp,wfn1(ioff),1)
        do jsp = 1, nspin
           call dcopy(w_grp%mydim,wfn1(ioff),1,fr_distr(ioff,jsp),1)
           call dmultiply_vec(ngrid,zxc(1,jsp,isp),fr_distr(ioff,jsp))
        enddo
     enddo
     call dgather(1,wfn1,vr)
     do jsp = 1, nspin
        call dgather(1,fr_distr(1,jsp),fr(1,jsp))
     enddo
     ipol = w_grp%mygr*w_grp%npes + ipol_pe + w_grp%inode
     if (ipol <= pol%ntr) then

        ! If necessary, update the Coulomb interaction.
        k1 = pol%tr(3,ipol)
        k2 = pol%tr(4,ipol)
        qkt_test = kpt%fk(:,k2) - kpt%fk(:,k1) - qkt

        if (dot_product(qkt_test,qkt_test) > tol_l) then
           if (peinf%master) then
              write(6,*) ' WARNING: q-point has changed '
              write(6,*) k1, kpt%fk(:,k1)
              write(6,*) k2, kpt%fk(:,k2)
              write(6,*) ' old q-vector ', qkt
              write(6,*) ' new q-vector ', kpt%fk(:,k2) - kpt%fk(:,k1)
           endif
           qkt = kpt%fk(:,k2) - kpt%fk(:,k1)
           if (gvec%per == 1) then
              call dcreate_coul_1D(peinf%inode,peinf%npes,peinf%comm,qkt(1),gvec,fft_box)
           elseif (gvec%per == 2) then
              call dcreate_coul_2D(gvec%bdot,qkt(1),fft_box)
           else
              call dcreate_coul_0D(gvec%bdot,qkt,fft_box)
           endif
        endif

        call dpoisson(gvec,vr,irp)
        write(itape) (vr(ii),ii=1,nr_buff), &
             ((fr(ii,jsp),ii=1,nr_buff),jsp=1,nspin)
     endif
     if (peinf%master .and. (mod(ipol,max(pol%nn/5,1)) == 0) ) then
        call stopwatch(.true.,' vvc/fvc calculation ')
        write(6,'(i10,a,i10,a,i2,a,i2)') ipol, ' out of ', pol%nn, &
             ' representation ', irp, ' spin ', isp
     endif
  enddo
  call flush(6)

  call dfinalize_FFT(peinf%inode,fft_box)

  deallocate(zxc)
  deallocate(fr_distr)
  deallocate(fr)
  deallocate(vr)
  deallocate(wfn2)
  deallocate(wfn1)

  call stopwatch(peinf%master,'Finished calculation of vvc/fvc.')

end subroutine dwpol_v
!===================================================================













!===================================================================
!
! Macros for the complex pre-processing. All source files with extension
! .F90z are pre-processed twice. Once with 1 and then again without 1.
! Most instances of capital "Z" are replaced by either lower case "z" or
! lower case "d" respectively with or without 1.
! BE CAREFUL WITH CAPITAL "Z" IN .F90z FILES !!! IT HAS SPECIAL MEANING!!!
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
! Constants
! Implicit functions
! External functions and subroutines
! Internal functions and subroutines
! Internal arrays
!===================================================================
!===================================================================
!
! Calculate wpol=X in real space and write it on scratch files.
! All the output of this subroutine is written to files TMPMATEL_*.
! Value of input parameters is not modified.
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
subroutine zwpol_v(gvec,kpt,pol,nolda,irp,nspin,nr_buff,itape,qcoord)

  use typedefs
  use mpi_module
  use xc_functionals
  use fft_module
  implicit none

  ! arguments
  ! real-space grid
  type (gspace), intent(inout) :: gvec
  ! k-points and electron wavefunctions (from DFT)
  type (kptinfo), intent(in) :: kpt
  ! TDDFT polarizability
  type (polinfo), intent(inout) :: pol
  ! true if LDA kernel is ignored
  logical, intent(in) :: nolda
  integer, intent(in) :: &
       irp, &         ! current representation
       nspin, &       ! number of spin channels
       nr_buff, &     ! length of output buffer (defines at how many
                      ! points the static limit is calculated)
       itape          ! output unit
  ! coordinates of current q-vector, in units of reciprocal lattice vectors
  real(dp) :: qcoord(3)

  ! local variables
  real(dp), parameter :: tol_l = 1.d-8
  type(xc_type) :: xc_lda
  integer :: ii, isp, jsp, info, ipol, ipe, m1, k1, m2, k2, &
       m2old, k2old, ispold, ngrid, ngrid_pe, ipol_pe, ioff
  real(dp) :: qkt(3), qkt_test(3)
  complex(dpc) :: xtmp
  complex(dpc) ,dimension(:), allocatable :: wfn1, wfn2, vr
  complex(dpc), dimension(:,:), allocatable :: fr, fr_distr
  complex(dpc), dimension(:,:,:), allocatable :: zxc
  real(dp), dimension(:,:,:), allocatable :: fxc

  ngrid_pe = w_grp%ldn * w_grp%npes
  ngrid = w_grp%mydim
  xtmp = zone / gvec%hcub

  !-------------------------------------------------------------------
  ! Calculate coulomb interaction,
  ! V_coul = 4*pi*e^2/q^2 (rydberg units, e^2 = 2).
  !
  qkt = zero
  call zinitialize_FFT(peinf%inode,fft_box)
  if (gvec%per == 1) then
     call zcreate_coul_1D(peinf%inode,peinf%npes,peinf%comm,qcoord(1),gvec,fft_box)
  elseif (gvec%per == 2) then
     call zcreate_coul_2D(gvec%bdot,qcoord(1),fft_box)
  else
     call zcreate_coul_0D(gvec%bdot,qcoord,fft_box)
  endif

  !-------------------------------------------------------------------
  ! Initialize arrays.
  !
  allocate(wfn1(ngrid_pe),stat=info)
  call alccheck('wfn1','wpol_v',ngrid_pe,info)
  wfn1 = zzero
  allocate(wfn2(ngrid),stat=info)
  call alccheck('wfn2','wpol_v',ngrid,info)

  allocate(vr(gvec%nr),stat=info)
  call alccheck('vr','wpol_v',gvec%nr,info)
  allocate(fr(gvec%nr,nspin),stat=info)
  call alccheck('fr','wpol_v',gvec%nr*nspin,info)
  allocate(fr_distr(ngrid_pe,nspin),stat=info)
  call alccheck('fr_distr','wpol_v',ngrid_pe*nspin,info)
  fr_distr = zzero

  !-------------------------------------------------------------------
  ! Calculate LDA kernel.
  !
  allocate(zxc(ngrid,nspin,nspin),stat=info)
  call alccheck('zxc','wpol_v',ngrid*nspin*nspin,info)
  zxc = zzero

  if (nolda) then
     if (r_grp%master) write(6,'(/,a,/,a,/,a,/)') repeat('=',65), &
          ' WARNING !!!! f_lda = zero ', repeat('=',65)
  else
     allocate(fxc(ngrid,nspin,nspin),stat=info)
     call alccheck('fxc','wpol_v',ngrid*nspin*nspin,info)
     fxc = zero
     do isp = 1, nspin
        do ii = 1, ngrid
           fxc(ii,isp,1) = kpt%rho(w_grp%offset+ii,isp)
        enddo
     enddo
     call xc_init(nspin,XC_LDA_X,XC_LDA_C_PZ,0,zero,one,.false.,xc_lda)
     xc_lda%has_grad = .false.
     call fxc_get(xc_lda,nspin,ngrid,1,fxc)
     zxc = fxc * zone
     call xc_end(xc_lda)
     deallocate(fxc)
  endif

  !-------------------------------------------------------------------
  ! Start calculating vvc, fvc.
  !
  call stopwatch(r_grp%master, &
       ' Memory allocation done. Start calculating vvc,fvc.')
  m2old = 0
  k2old = 0
  ispold = 0
  do ipol_pe = 1, pol%ntr, r_grp%npes
     do ipe = 0, w_grp%npes - 1
        ipol = w_grp%mygr*w_grp%npes + ipol_pe + ipe
        if (ipol > pol%ntr) exit
        ioff = w_grp%ldn*ipe + 1
        m2 = pol%tr(2,ipol)
        k2 = pol%tr(4,ipol)
        isp = 1
        if (ipol > pol%n_up) isp = 2
        if (m2 /= m2old .or. isp /= ispold .or. k2 /= k2old) then
           ii = kpt%wfn(isp,k2)%map(m2)
           wfn2(1:ngrid) = kpt%wfn(isp,k2)%zwf(1:ngrid,ii)
           m2old = m2
           k2old = k2
           ispold = isp
        endif
        m1 = pol%tr(1,ipol)
        k1 = pol%tr(3,ipol)
        ii = kpt%wfn(isp,k1)%map(m1)
        wfn1(ioff:ioff + ngrid - 1) = conjg(kpt%wfn(isp,k1)%zwf(1:ngrid,ii))
        call zmultiply_vec(ngrid,wfn2,wfn1(ioff))
        call zscal(ngrid,xtmp,wfn1(ioff),1)
        do jsp = 1, nspin
           call zcopy(w_grp%mydim,wfn1(ioff),1,fr_distr(ioff,jsp),1)
           call zmultiply_vec(ngrid,zxc(1,jsp,isp),fr_distr(ioff,jsp))
        enddo
     enddo
     call zgather(1,wfn1,vr)
     do jsp = 1, nspin
        call zgather(1,fr_distr(1,jsp),fr(1,jsp))
     enddo
     ipol = w_grp%mygr*w_grp%npes + ipol_pe + w_grp%inode
     if (ipol <= pol%ntr) then

        ! If necessary, update the Coulomb interaction.
        k1 = pol%tr(3,ipol)
        k2 = pol%tr(4,ipol)
        qkt_test = kpt%fk(:,k2) - kpt%fk(:,k1) - qkt

        if (dot_product(qkt_test,qkt_test) > tol_l) then
           if (peinf%master) then
              write(6,*) ' WARNING: q-point has changed '
              write(6,*) k1, kpt%fk(:,k1)
              write(6,*) k2, kpt%fk(:,k2)
              write(6,*) ' old q-vector ', qkt
              write(6,*) ' new q-vector ', kpt%fk(:,k2) - kpt%fk(:,k1)
           endif
           qkt = kpt%fk(:,k2) - kpt%fk(:,k1)
           if (gvec%per == 1) then
              call zcreate_coul_1D(peinf%inode,peinf%npes,peinf%comm,qkt(1),gvec,fft_box)
           elseif (gvec%per == 2) then
              call zcreate_coul_2D(gvec%bdot,qkt(1),fft_box)
           else
              call zcreate_coul_0D(gvec%bdot,qkt,fft_box)
           endif
        endif

        call zpoisson(gvec,vr,irp)
        write(itape) (vr(ii),ii=1,nr_buff), &
             ((fr(ii,jsp),ii=1,nr_buff),jsp=1,nspin)
     endif
     if (peinf%master .and. (mod(ipol,max(pol%nn/5,1)) == 0) ) then
        call stopwatch(.true.,' vvc/fvc calculation ')
        write(6,'(i10,a,i10,a,i2,a,i2)') ipol, ' out of ', pol%nn, &
             ' representation ', irp, ' spin ', isp
     endif
  enddo
  call flush(6)

  call zfinalize_FFT(peinf%inode,fft_box)

  deallocate(zxc)
  deallocate(fr_distr)
  deallocate(fr)
  deallocate(vr)
  deallocate(wfn2)
  deallocate(wfn1)

  call stopwatch(peinf%master,'Finished calculation of vvc/fvc.')

end subroutine zwpol_v
!===================================================================
