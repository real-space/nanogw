












!===================================================================
!
! Macros for the complex pre-processing. All source files with extension
! .F90z are pre-processed twice. Once with CPLX and then again without CPLX.
! Most instances of capital "Z" are replaced by either lower case "z" or
! lower case "d" respectively with or without CPLX.
! BE CAREFUL WITH CAPITAL "Z" IN .F90z FILES !!! IT HAS SPECIAL MEANING!!!
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
! Constants
! Implicit functions
! External functions and subroutines
! Internal functions and subroutines
! Internal arrays
!===================================================================
!===================================================================
!
! Add the static remainder to sig%scsdiag (correlation) and
! sig%sgsdiag (vertex). The static remainder in correlation is the
! integral term in Eq. B3 of Tiago & Chelikowsky, PRB (2006). The
! static remainder in vertex is the integral term in Eq. B6 of
! Tiago & Chelikowsky, PRB (2006).
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
subroutine dstatic(sig,wfn,ngrid,vr,fr)

  use typedefs
  use mpi_module
  implicit none

  ! arguments
  ! self-energy
  type (siginfo), intent(inout) :: sig
  ! electron wavefunctions for the current k-point and spin channel
  type (wavefunction), intent(in) :: wfn
  ! number of points where the static potentials were calculated
  integer, intent(in) :: ngrid
  ! static potentials, the r' integrals in Eq. B3 and B6 respectively
  real(dp), intent(in) :: vr(ngrid), fr(ngrid)

  ! local variables
  integer :: isig, i1, i2, icol_pe, ipe
  real(dp) :: wfn1(ngrid)
  real(dp), external :: ddot
  real(dp), allocatable :: xdum(:,:)

  !-------------------------------------------------------------------
  ! Start with self-energy diagonal matrix elements.
  !
  if (sig%ndiag_s > 0) then
     allocate(xdum(sig%ndiag_s,2),stat=i1)
     call alccheck('xdum','static',sig%ndiag_s*2,i1)
     xdum = zero
     do icol_pe = 1, sig%ndiag_s, peinf%npes
        do ipe = 0, w_grp%npes - 1
           isig = icol_pe + ipe + &
                w_grp%npes * w_grp%mygr + r_grp%npes * r_grp%mygr
           if (isig > sig%ndiag_s) cycle
           i1 = sig%map(sig%diag(isig))

           call dcopy(ngrid,wfn%dwf(1,wfn%map(i1)),1,wfn1,1)
           wfn1 = (wfn1)
           call dmultiply_vec(ngrid,wfn%dwf(1,wfn%map(i1)),wfn1)

           xdum(isig,1) = xdum(isig,1) + ddot(ngrid,wfn1,1,vr,1)
           xdum(isig,2) = xdum(isig,2) + ddot(ngrid,wfn1,1,fr,1)
        enddo
     enddo
     call dpsum(sig%ndiag_s*2,peinf%npes,peinf%comm,xdum)
     sig%scsdiag = sig%scsdiag + xdum(:,1)
     sig%sgsdiag = sig%sgsdiag + xdum(:,2)
     deallocate(xdum)
  endif

  !-------------------------------------------------------------------
  ! Now, do self-energy off-diagonal matrix elements.
  !
  if (sig%noffd_s > 0) then
     allocate(xdum(sig%noffd_s,2),stat=i1)
     call alccheck('xdum','static',sig%noffd_s*2,i1)
     xdum = zero
     do icol_pe = 1, sig%noffd_s, peinf%npes
        do ipe = 0, w_grp%npes - 1
           isig = icol_pe + ipe + &
                w_grp%npes * w_grp%mygr + r_grp%npes * r_grp%mygr
           if (isig > sig%noffd_s) cycle
           i1 = sig%map(sig%off1(isig))
           i2 = sig%map(sig%off2(isig))
           if ( wfn%irep(i1) /= wfn%irep(i2) ) cycle

           call dcopy(ngrid,wfn%dwf(1,wfn%map(i1)),1,wfn1,1)
           wfn1 = (wfn1)
           call dmultiply_vec(ngrid,wfn%dwf(1,wfn%map(i2)),wfn1)

           xdum(isig,1) = xdum(isig,1) + ddot(ngrid,wfn1,1,vr,1)
           xdum(isig,2) = xdum(isig,2) + ddot(ngrid,wfn1,1,fr,1)
        enddo
     enddo
     call dpsum(sig%noffd_s*2,peinf%npes,peinf%comm,xdum)
     sig%scsoffd = sig%scsoffd + xdum(:,1)
     sig%sgsoffd = sig%sgsoffd + xdum(:,2)
     deallocate(xdum)
  endif

end subroutine dstatic
!===================================================================













!===================================================================
!
! Macros for the complex pre-processing. All source files with extension
! .F90z are pre-processed twice. Once with 1 and then again without 1.
! Most instances of capital "Z" are replaced by either lower case "z" or
! lower case "d" respectively with or without 1.
! BE CAREFUL WITH CAPITAL "Z" IN .F90z FILES !!! IT HAS SPECIAL MEANING!!!
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
! Constants
! Implicit functions
! External functions and subroutines
! Internal functions and subroutines
! Internal arrays
!===================================================================
!===================================================================
!
! Add the static remainder to sig%scsdiag (correlation) and
! sig%sgsdiag (vertex). The static remainder in correlation is the
! integral term in Eq. B3 of Tiago & Chelikowsky, PRB (2006). The
! static remainder in vertex is the integral term in Eq. B6 of
! Tiago & Chelikowsky, PRB (2006).
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
subroutine zstatic(sig,wfn,ngrid,vr,fr)

  use typedefs
  use mpi_module
  implicit none

  ! arguments
  ! self-energy
  type (siginfo), intent(inout) :: sig
  ! electron wavefunctions for the current k-point and spin channel
  type (wavefunction), intent(in) :: wfn
  ! number of points where the static potentials were calculated
  integer, intent(in) :: ngrid
  ! static potentials, the r' integrals in Eq. B3 and B6 respectively
  complex(dpc), intent(in) :: vr(ngrid), fr(ngrid)

  ! local variables
  integer :: isig, i1, i2, icol_pe, ipe
  complex(dpc) :: wfn1(ngrid)
  complex(dpc), external :: zdot_u
  complex(dpc), allocatable :: xdum(:,:)

  !-------------------------------------------------------------------
  ! Start with self-energy diagonal matrix elements.
  !
  if (sig%ndiag_s > 0) then
     allocate(xdum(sig%ndiag_s,2),stat=i1)
     call alccheck('xdum','static',sig%ndiag_s*2,i1)
     xdum = zzero
     do icol_pe = 1, sig%ndiag_s, peinf%npes
        do ipe = 0, w_grp%npes - 1
           isig = icol_pe + ipe + &
                w_grp%npes * w_grp%mygr + r_grp%npes * r_grp%mygr
           if (isig > sig%ndiag_s) cycle
           i1 = sig%map(sig%diag(isig))

           call zcopy(ngrid,wfn%zwf(1,wfn%map(i1)),1,wfn1,1)
           wfn1 = conjg(wfn1)
           call zmultiply_vec(ngrid,wfn%zwf(1,wfn%map(i1)),wfn1)

           xdum(isig,1) = xdum(isig,1) + zdot_u(ngrid,wfn1,1,vr,1)
           xdum(isig,2) = xdum(isig,2) + zdot_u(ngrid,wfn1,1,fr,1)
        enddo
     enddo
     call zpsum(sig%ndiag_s*2,peinf%npes,peinf%comm,xdum)
     sig%scsdiag = sig%scsdiag + xdum(:,1)
     sig%sgsdiag = sig%sgsdiag + xdum(:,2)
     deallocate(xdum)
  endif

  !-------------------------------------------------------------------
  ! Now, do self-energy off-diagonal matrix elements.
  !
  if (sig%noffd_s > 0) then
     allocate(xdum(sig%noffd_s,2),stat=i1)
     call alccheck('xdum','static',sig%noffd_s*2,i1)
     xdum = zzero
     do icol_pe = 1, sig%noffd_s, peinf%npes
        do ipe = 0, w_grp%npes - 1
           isig = icol_pe + ipe + &
                w_grp%npes * w_grp%mygr + r_grp%npes * r_grp%mygr
           if (isig > sig%noffd_s) cycle
           i1 = sig%map(sig%off1(isig))
           i2 = sig%map(sig%off2(isig))
           if ( wfn%irep(i1) /= wfn%irep(i2) ) cycle

           call zcopy(ngrid,wfn%zwf(1,wfn%map(i1)),1,wfn1,1)
           wfn1 = conjg(wfn1)
           call zmultiply_vec(ngrid,wfn%zwf(1,wfn%map(i2)),wfn1)

           xdum(isig,1) = xdum(isig,1) + zdot_u(ngrid,wfn1,1,vr,1)
           xdum(isig,2) = xdum(isig,2) + zdot_u(ngrid,wfn1,1,fr,1)
        enddo
     enddo
     call zpsum(sig%noffd_s*2,peinf%npes,peinf%comm,xdum)
     sig%scsoffd = sig%scsoffd + xdum(:,1)
     sig%sgsoffd = sig%sgsoffd + xdum(:,2)
     deallocate(xdum)
  endif

end subroutine zstatic
!===================================================================
