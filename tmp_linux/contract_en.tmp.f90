












!===================================================================
!
! Macros for the complex pre-processing. All source files with extension
! .F90z are pre-processed twice. Once with CPLX and then again without CPLX.
! Most instances of capital "Z" are replaced by either lower case "z" or
! lower case "d" respectively with or without CPLX.
! BE CAREFUL WITH CAPITAL "Z" IN .F90z FILES !!! IT HAS SPECIAL MEANING!!!
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
! Constants
! Implicit functions
! External functions and subroutines
! Internal functions and subroutines
! Internal arrays
!===================================================================
!===================================================================
!
! Compute conjg(pot_l) * pot_r / E. This operation is performed
! several times during a self-energy calculation. See for example
! Eq. 24, 30 of Tiago & Chelikowsky, PRB 73, 205334
!
!
! INPUT:
!    nn : number of polarizability poles
!    nen : number of energy values
!    eni : energy values
!    esign : sign in denominator
!    ecut : Lorentzian cutoff
!    ecut2 = ecut*ecut
!    occfac : occupancy of orbitals
!    eig : polarizability eigenvalues
!    pot_r : V^1 potential
!    pot_l : V^2 potential
!
! OUTPUT:
!    sum0 : final sum
!
!   sum0(1,i) = sum_(s=1,nn) conjg(pot_l(s)) * pot_r(s) * occfac *
!        [ eni(i) + eig(s) ] / [ { eni(i) + eig(s) }^2 + ecut2 ]
!
!   sum0(2,i) = sum_(s=1,nn) conjg(pot_l(s)) * pot_r(s) * occfac *
!        ecut * occfac / [ { eni(i) + eig(s) }^2 + ecut2 ]
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
subroutine dcontract_en(nn,nen,eni,esign,ecut,ecut2,occfac,eig,pot_r,pot_l,sum0)
  use myconstants
  implicit none

  ! arguments
  integer, intent(in) :: nn, nen
  real(dp), intent(in) :: eni(nen), esign, ecut2, ecut, occfac
  real(dp), intent(in) :: eig(nn)
  real(dp), intent(in) :: pot_r(nn), pot_l(nn)
  real(dp), intent(inout) :: sum0(2,nen)

  ! local variables
  integer :: jj, ien
  real(dp) :: edenr(nen), edeni(nen), pot_prod(nn), ecutsgn, zoccfac

  if (nn <= 0) return

  ecutsgn = one * ecut * esign
  zoccfac = one * occfac
  call dscal(nn,esign,eig,1)
  call dcopy(nn,pot_l,1,pot_prod,1)
  pot_prod = (pot_prod)
  call dmultiply_vec(nn,pot_r,pot_prod)
  ! pot_prod = zoccfac*pot_l(1:nn)*pot_r(1:nn)
  call dscal(nn,zoccfac,pot_prod,1)
  do jj = 1, nn
     edenr = one*( eni + eig(jj) )
     edenr = edenr**2 + ecut2
     do ien = 1, nen
        edeni(ien) = ecutsgn/edenr(ien)
        edenr(ien) = (eni(ien) + eig(jj))/edenr(ien)
     enddo
     ! sum0(1,1:nen) = sum0(1,1:nen) + pot_prod(jj)*edenr(1:nen)
     ! sum0(2,1:nen) = sum0(2,1:nen) + pot_prod(jj)*edenr(1:nen)
     call daxpy(nen,pot_prod(jj),edenr,1,sum0(1,1),2)
     call daxpy(nen,pot_prod(jj),edeni,1,sum0(2,1),2)
  enddo
  call dscal(nn,esign,eig,1)

end subroutine dcontract_en
!===================================================================













!===================================================================
!
! Macros for the complex pre-processing. All source files with extension
! .F90z are pre-processed twice. Once with 1 and then again without 1.
! Most instances of capital "Z" are replaced by either lower case "z" or
! lower case "d" respectively with or without 1.
! BE CAREFUL WITH CAPITAL "Z" IN .F90z FILES !!! IT HAS SPECIAL MEANING!!!
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
! Constants
! Implicit functions
! External functions and subroutines
! Internal functions and subroutines
! Internal arrays
!===================================================================
!===================================================================
!
! Compute conjg(pot_l) * pot_r / E. This operation is performed
! several times during a self-energy calculation. See for example
! Eq. 24, 30 of Tiago & Chelikowsky, PRB 73, 205334
!
!
! INPUT:
!    nn : number of polarizability poles
!    nen : number of energy values
!    eni : energy values
!    esign : sign in denominator
!    ecut : Lorentzian cutoff
!    ecut2 = ecut*ecut
!    occfac : occupancy of orbitals
!    eig : polarizability eigenvalues
!    pot_r : V^1 potential
!    pot_l : V^2 potential
!
! OUTPUT:
!    sum0 : final sum
!
!   sum0(1,i) = sum_(s=1,nn) conjg(pot_l(s)) * pot_r(s) * occfac *
!        [ eni(i) + eig(s) ] / [ { eni(i) + eig(s) }^2 + ecut2 ]
!
!   sum0(2,i) = sum_(s=1,nn) conjg(pot_l(s)) * pot_r(s) * occfac *
!        ecut * occfac / [ { eni(i) + eig(s) }^2 + ecut2 ]
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
subroutine zcontract_en(nn,nen,eni,esign,ecut,ecut2,occfac,eig,pot_r,pot_l,sum0)
  use myconstants
  implicit none

  ! arguments
  integer, intent(in) :: nn, nen
  real(dp), intent(in) :: eni(nen), esign, ecut2, ecut, occfac
  real(dp), intent(in) :: eig(nn)
  complex(dpc), intent(in) :: pot_r(nn), pot_l(nn)
  complex(dpc), intent(inout) :: sum0(2,nen)

  ! local variables
  integer :: jj, ien
  complex(dpc) :: edenr(nen), edeni(nen), pot_prod(nn), ecutsgn, zoccfac

  if (nn <= 0) return

  ecutsgn = zone * ecut * esign
  zoccfac = zone * occfac
  call dscal(nn,esign,eig,1)
  call zcopy(nn,pot_l,1,pot_prod,1)
  pot_prod = conjg(pot_prod)
  call zmultiply_vec(nn,pot_r,pot_prod)
  ! pot_prod = zoccfac*pot_l(1:nn)*pot_r(1:nn)
  call zscal(nn,zoccfac,pot_prod,1)
  do jj = 1, nn
     edenr = zone*( eni + eig(jj) )
     edenr = edenr**2 + ecut2
     do ien = 1, nen
        edeni(ien) = ecutsgn/edenr(ien)
        edenr(ien) = (eni(ien) + eig(jj))/edenr(ien)
     enddo
     ! sum0(1,1:nen) = sum0(1,1:nen) + pot_prod(jj)*edenr(1:nen)
     ! sum0(2,1:nen) = sum0(2,1:nen) + pot_prod(jj)*edenr(1:nen)
     call zaxpy(nen,pot_prod(jj),edenr,1,sum0(1,1),2)
     call zaxpy(nen,pot_prod(jj),edeni,1,sum0(2,1),2)
  enddo
  call dscal(nn,esign,eig,1)

end subroutine zcontract_en
!===================================================================
