












!===================================================================
!
! Macros for the complex pre-processing. All source files with extension
! .F90z are pre-processed twice. Once with CPLX and then again without CPLX.
! Most instances of capital "Z" are replaced by either lower case "z" or
! lower case "d" respectively with or without CPLX.
! BE CAREFUL WITH CAPITAL "Z" IN .F90z FILES !!! IT HAS SPECIAL MEANING!!!
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
! Constants
! Implicit functions
! External functions and subroutines
! Internal functions and subroutines
! Internal arrays
!===================================================================
!===================================================================
!
! Global sum routine using the MPI_ALLREDUCE primitive.
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
subroutine dpsum(ndim,npes,comm,buffer)
  use myconstants
  implicit none
  include 'mpif.h'
  ! dimension of array, number of PEs, 1 communicator
  integer, intent(in) :: ndim, npes, comm
  ! input: array to be summed, local
  ! output: array summed across all PEs, global
  real(dp), intent(inout) :: buffer(ndim)

  real(dp) :: work(MAXSIZE_MPI)
  integer :: info, ioff, nblock, mdim, iblock

  !-------------------------------------------------------------------
  if (npes == 1) return
  if (ndim < 1) return
  ioff = 1
  mdim = MAXSIZE_MPI
  if (ndim > MAXSIZE_MPI) then
     nblock = ndim / MAXSIZE_MPI
     do iblock = 1, nblock
        call MPI_ALLREDUCE(buffer(ioff),work,mdim, &
             MPI_DOUBLE_PRECISION,MPI_SUM,comm,info)
        call dcopy(mdim,work,1,buffer(ioff),1)
        ioff = ioff + mdim
     enddo
  endif
  mdim = ndim - ioff + 1
  call MPI_ALLREDUCE(buffer(ioff),work,mdim, &
       MPI_DOUBLE_PRECISION,MPI_SUM,comm,info)
  call dcopy(mdim,work,1,buffer(ioff),1)

end subroutine dpsum
!===================================================================













!===================================================================
!
! Macros for the complex pre-processing. All source files with extension
! .F90z are pre-processed twice. Once with 1 and then again without 1.
! Most instances of capital "Z" are replaced by either lower case "z" or
! lower case "d" respectively with or without 1.
! BE CAREFUL WITH CAPITAL "Z" IN .F90z FILES !!! IT HAS SPECIAL MEANING!!!
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
! Constants
! Implicit functions
! External functions and subroutines
! Internal functions and subroutines
! Internal arrays
!===================================================================
!===================================================================
!
! Global sum routine using the MPI_ALLREDUCE primitive.
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
subroutine zpsum(ndim,npes,comm,buffer)
  use myconstants
  implicit none
  include 'mpif.h'
  ! dimension of array, number of PEs, 1 communicator
  integer, intent(in) :: ndim, npes, comm
  ! input: array to be summed, local
  ! output: array summed across all PEs, global
  complex(dpc), intent(inout) :: buffer(ndim)

  complex(dpc) :: work(MAXSIZE_MPI)
  integer :: info, ioff, nblock, mdim, iblock

  !-------------------------------------------------------------------
  if (npes == 1) return
  if (ndim < 1) return
  ioff = 1
  mdim = MAXSIZE_MPI
  if (ndim > MAXSIZE_MPI) then
     nblock = ndim / MAXSIZE_MPI
     do iblock = 1, nblock
        call MPI_ALLREDUCE(buffer(ioff),work,mdim, &
             MPI_DOUBLE_COMPLEX,MPI_SUM,comm,info)
        call zcopy(mdim,work,1,buffer(ioff),1)
        ioff = ioff + mdim
     enddo
  endif
  mdim = ndim - ioff + 1
  call MPI_ALLREDUCE(buffer(ioff),work,mdim, &
       MPI_DOUBLE_COMPLEX,MPI_SUM,comm,info)
  call zcopy(mdim,work,1,buffer(ioff),1)

end subroutine zpsum
!===================================================================
