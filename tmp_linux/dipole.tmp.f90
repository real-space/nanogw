












!===================================================================
!
! Macros for the complex pre-processing. All source files with extension
! .F90z are pre-processed twice. Once with CPLX and then again without CPLX.
! Most instances of capital "Z" are replaced by either lower case "z" or
! lower case "d" respectively with or without CPLX.
! BE CAREFUL WITH CAPITAL "Z" IN .F90z FILES !!! IT HAS SPECIAL MEANING!!!
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
! Constants
! Implicit functions
! External functions and subroutines
! Internal functions and subroutines
! Internal arrays
!===================================================================
!===================================================================
!
! Calculate dipole matrix elements for all wavefunctions available
! and the three cartesian directions. In bulk systems, the dipoles
! are calculated along the three primitive lattice directions.
!
! Dipole matrix elements are calculated between all possible pairs
! of occupied (iv) and unoccupied (ic) states.
! Dipole given in atomic units (Bohr radius = 1):
!
! dip(iv,ic,1) = < v | x | c >
! dip(iv,ic,2) = < v | y | c >
! dip(iv,ic,3) = < v | z | c >
!
! In periodic systems, calculates dipole matrix elements between
! occupied (i) and empty (j) orbitals using the velocity operator:
!
! dip_(i,j,:) = < i | v(:) | j > / (E_j - E_i)
!
! For non-periodic systems, the matrix elements dip_(i,j) are
! identical to matrix elements of the position operator (and
! hence dipole):
!
! dip_(i,j,:) = < i | r(:) | j >
!
! For periodic systems, the identity above does not hold because
! the orbitals are extended and matrix elements involving the
! position operator are ill defined. Corrections due to the non
! -local pseudo-potentials are included in the velocity operator:
!
! v = p/m + (i/hbar) * V_nl (r,r') * [r' - r]
!
! OUTPUT:
!    wfn%ndip : number of dipole matrix elements (usually the
!               product of number of unoccupied and occupied orbitals)
!    wfn%mapd : address of orbitals (iv,ic) for each dipole
!    wfn%ddipole : dipole matrix elements
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
subroutine dget_dipole(gvec,wfn,ik,kvec,wfn_occ)

  use typedefs
  use mpi_module
  use fft_module
  use fd_module
  use psp_module
  implicit none

  ! arguments
  type (gspace), intent(inout) :: gvec
  type (wavefunction), intent(inout) :: wfn
  ! index of current k-point
  integer, intent(in) :: ik
  ! coordinates of current k-point, in units of reciprocal lattice vectors
  real(dp), intent(in) :: kvec(3)
  ! occupancy of electronic orbitals
  real(dp), intent(in) :: wfn_occ(wfn%nstate)

  ! local variables
  character (len=800) :: lastwords
  logical :: use_velocity
  integer :: ii, jj, iv, ic, jv, jc, nv, nc, mv, mc, irp, gr(3), &
       occ_bnds(4), ngrid, off, ioff, joff, mygrid, jj_loc, icol_pe, ipe
  real(dp) :: vwr, etr
  real(dp) :: rr(3)
  ! Cartesian coordinates of k-vector
  real(dp) :: kcart(3)
  ! threshold in the energy denominator
  real(dp), parameter :: EN_DEG = 1.0d-3

  integer, allocatable :: chi_tmp(:)
  real(dp), allocatable :: wfn1(:), wfn2(:), wfn_loc(:), &
       wvec_loc(:,:), w_distr(:,:), wvec(:,:), dip(:,:,:)
  real(dp), external :: ddot

  call dmatvec3('N',gvec%bvec,kvec,kcart)

  occ_bnds = 0
  occ_bnds(1) = wfn%nstate
  do ii = 1, wfn%nstate
     if ( wfn%map(ii) == 0 ) cycle
     if ( ii < occ_bnds(1) ) occ_bnds(1) = ii
  enddo
  do ii = 1, wfn%nstate
     if ( wfn%map(ii) == 0 ) cycle
     if ( ii > occ_bnds(4) ) occ_bnds(4) = ii
  enddo
  do ii = 1, wfn%nstate
     if ( wfn%map(ii) == 0 ) cycle
     if (wfn_occ(ii) > tol_occ)  occ_bnds(2) = ii
  enddo
  do ii = wfn%nstate, 1, -1
     if ( wfn%map(ii) == 0 ) cycle
     if (wfn_occ(ii) < one - tol_occ) occ_bnds(3) = ii
  enddo

  if (gvec%per > 0) then
     use_velocity = .true.
  else
     use_velocity = .false.
  endif

  ngrid = w_grp%ldn * w_grp%npes * gvec%syms%ntrans
  off = w_grp%offset
  mygrid = w_grp%mydim
  nv = occ_bnds(2) - occ_bnds(1) + 1
  nc = occ_bnds(4) - occ_bnds(3) + 1
  if (nv < 1 .or. nc < 1) then
     wfn%ndip = 0
     return
  endif
  allocate(dip(nv,nc,3))
  dip = zero
  !-------------------------------------------------------------------
  ! Allocate data.
  !
  allocate(wvec(3,ngrid),stat=ii)
  call alccheck('wvec','dipole',3*ngrid,ii)
  wvec = zero
  allocate(wfn1(ngrid),stat=ii)
  call alccheck('wfn1','dipole',ngrid,ii)
  allocate(wfn2(w_grp%ldn*gvec%syms%ntrans),stat=ii)
  call alccheck('wfn2','dipole',w_grp%ldn*gvec%syms%ntrans,ii)
  wfn2 = zero
  allocate(chi_tmp(gvec%syms%ntrans))
  if (use_velocity) then
     allocate(w_distr(w_grp%ldn,w_grp%npes),stat=ii)
     call alccheck('w_distr','dipole',w_grp%ldn*w_grp%npes,ii)
     w_distr = zero
     allocate(wvec_loc(3,w_grp%nr * gvec%syms%ntrans),stat=ii)
     call alccheck('wvec_loc','dipole',3*w_grp%nr*gvec%syms%ntrans,ii)
     allocate(wfn_loc(w_grp%nr),stat=ii)
     call alccheck('wfn_loc','dipole',w_grp%nr,ii)
  else
     !
     ! Calculate wvec = (r1,r2,r3) in the real space grid.
     !
     do irp = 1, gvec%syms%ntrans
        do ii = 1, mygrid
           call unfold(gvec%r(1,ii+off),gvec%syms%trans(1,1,irp),gvec%shift,gr)
           rr = (real(gr,dp) + gvec%shift(:) )*gvec%step(:)
           jj = w_grp%ldn*(irp-1) + ii
           wvec(:,jj) = rr
        enddo
     enddo
  endif


  !-------------------------------------------------------------------
  ! Start calculation.
  !
  do icol_pe = 1, nc, peinf%npes
     if (use_velocity) then
        do ipe = 0, w_grp%npes - 1
           ic = icol_pe + ipe + &
                w_grp%npes * w_grp%mygr + r_grp%npes * r_grp%mygr
           if (ic > nc) cycle
           mc = ic + occ_bnds(3) - 1
           jc = wfn%map(mc)
           if (jc == 0) then
              write(lastwords,*) ' ERROR in dipole: could not find ', &
                   'wavefunction ',mc,' in memory! ',ic, jc
              call die(lastwords)
           endif
           call dcopy(mygrid,wfn%dwf(1,jc),1,w_distr(1,ipe+1),1)
        enddo
        call dgather(1,w_distr,wfn_loc)
     endif
     ic = icol_pe + w_grp%inode + &
          w_grp%npes * w_grp%mygr + r_grp%npes * r_grp%mygr
     if (ic <= nc) then
        mc = ic + occ_bnds(3) - 1
        jc = wfn%map(mc)
        if (jc == 0) then
           write(lastwords,*) ' ERROR in dipole: could not find ', &
                'wavefunction ', mc, ' in memory! ', ic, jc
           call die(lastwords)
        endif
        if (use_velocity) then
           irp = wfn%jrep(0,mc)
           if (fd%norder < 0) then
              call dget_grad_FFT(gvec,wfn_loc,wvec_loc,irp)
           else
              call dget_grad_fd(gvec%syms,wfn_loc,wvec_loc,irp)
           endif
        else
           do ipe = 0, w_grp%npes - 1
              ic = icol_pe + ipe + &
                   w_grp%npes * w_grp%mygr + r_grp%npes * r_grp%mygr
              if (ic > nc) cycle
              mc = ic + occ_bnds(3) - 1
              jc = wfn%map(mc)
              if (jc == 0) then
                 write(lastwords,*) ' ERROR in dipole: could not find ', &
                      'wavefunction ',mc,' in memory! ',ic, jc
                 call die(lastwords)
              endif
              do irp = 1, gvec%syms%ntrans
                 do ii = 1, mygrid
                    jj = w_grp%ldn*((irp-1)*w_grp%npes + ipe) + ii
                    wfn1(jj) = wfn%dwf(ii,jc) * real(gvec%syms%chi(wfn%jrep(0,mc),irp),dp)
                 enddo
              enddo
           enddo
        endif
     endif
     if (use_velocity) then
        do irp = 1, gvec%syms%ntrans
           ii = w_grp%ldn*w_grp%npes*(irp-1) + 1
           jj = w_grp%nr * ( irp - 1 ) + 1
           call dscatter(3,wvec(1,ii),wvec_loc(1,jj))
        enddo
     endif
     do ipe = 0, w_grp%npes - 1
        ic = icol_pe + ipe + w_grp%npes * w_grp%mygr + r_grp%npes * r_grp%mygr
        if (ic > nc) cycle
        do iv = 1, nv
           mv = iv + occ_bnds(1) - 1
           jv = wfn%map(mv)
           if (jv == 0) then
              write(lastwords,*) ' ERROR in dipole : orbital ', mv, &
                   ' not found ! STOP'
              call die(lastwords)
           endif
           irp = wfn%jrep(0,mv)
           chi_tmp = gvec%syms%chi(irp,:)
           do irp = 1, gvec%syms%ntrans
              do ii = 1, mygrid
                 jj = w_grp%ldn*(irp-1) + ii
                 if (use_velocity) then
                    jj_loc = w_grp%nr * ( irp - 1 ) + ii + off
                    wfn2(jj) =  chi_tmp(gvec%rtrans(jj_loc)) * &
                         wfn%dwf(gvec%rindex(jj_loc)-off,jv)
                 else
                    wfn2(jj) = wfn%dwf(ii,jv) * real(chi_tmp(irp),dp)
                 endif
                 wfn2(jj) = (wfn2(jj))
              enddo
           enddo
           if (use_velocity) then
              !  dip(iv,ic,:)  --> < v | Grad/m + i k/m | c >
              do irp = 1, gvec%syms%ntrans
                 ioff = ((irp-1)*w_grp%npes + ipe)*w_grp%ldn + 1
                 joff = (irp-1)*w_grp%ldn + 1
                 call dgemv('N',3,mygrid,one,wvec(1,ioff),3,wfn2(joff),1,zero,rr,1)
                 dip(iv,ic,:) = dip(iv,ic,:) + rr * two
              enddo
           else
              !  dip(iv,ic,:)  --> < v | r | c >
              do irp = 1, gvec%syms%ntrans
                 ioff = ((irp-1)*w_grp%npes + ipe)*w_grp%ldn + 1
                 joff = (irp-1)*w_grp%ldn + 1
                 call dmultiply_vec(mygrid,wfn1(ioff),wfn2(joff))
                 ioff = (irp-1)*w_grp%ldn + 1
                 call dgemv('N',3,mygrid,one,wvec(1,ioff),3,wfn2(joff),1,zero,rr,1)
                 dip(iv,ic,:) = dip(iv,ic,:) + rr
              enddo
           endif
        enddo  !iv
     enddo  ! do ipe = 0, w_grp%npes-1
  enddo  !ic


  !-------------------------------------------------------------------
  ! In periodic systems, must add the energy denominator and the
  ! non-local correction.
  !
  if (use_velocity) then

     do ii = 1, type_num
        call dnonloc(gvec,psp(ii),wfn,occ_bnds,ii,kcart,dip)
     enddo

     do ic = occ_bnds(3), occ_bnds(4)
        jc = ic - occ_bnds(3) + 1
        vwr = wfn%e1(ic)
        do iv = occ_bnds(1), occ_bnds(2)
           jv = iv - occ_bnds(1) + 1
           etr = vwr - wfn%e1(iv)
           if ( etr < EN_DEG ) then
              if (peinf%master) then
                 write(6,*) ' WARNING: states ',ic,iv,' are almost degenerate!'
                 write(6,*) 'Cannot calculate velocity matrix element. ', &
                      'Reset it to zero.'
              endif
              dip(jv,jc,:) = zero
           else
              dip(jv,jc,:) = dip(jv,jc,:) / etr
           endif
        enddo
     enddo
  endif

  !-------------------------------------------------------------------
  ! Done. Deallocate memory and save data.
  !
  deallocate(wvec)
  deallocate(wfn1)
  deallocate(wfn2)
  deallocate(chi_tmp)
  if (use_velocity) then
     deallocate(wvec_loc)
     deallocate(wfn_loc)
     deallocate(w_distr)
  endif
  call dpsum(nv*nc*3,peinf%npes,peinf%comm,dip)

  wfn%ndip = nv*nc
  allocate(wfn%ddipole(wfn%ndip,3))
  allocate(wfn%mapd(4,wfn%ndip))
  ii = 0
  do ic = occ_bnds(3),occ_bnds(4)
     do iv = occ_bnds(1),occ_bnds(2)
        ii = ii + 1
        wfn%mapd(1,ii) = iv
        wfn%mapd(2,ii) = ic
        wfn%mapd(3,ii) = ik
        wfn%mapd(4,ii) = ik
        wfn%ddipole(ii,:) = dip(iv-occ_bnds(1)+1,ic-occ_bnds(3)+1,:)
     enddo
  enddo

  deallocate(dip)

end subroutine dget_dipole
!===============================================================













!===================================================================
!
! Macros for the complex pre-processing. All source files with extension
! .F90z are pre-processed twice. Once with 1 and then again without 1.
! Most instances of capital "Z" are replaced by either lower case "z" or
! lower case "d" respectively with or without 1.
! BE CAREFUL WITH CAPITAL "Z" IN .F90z FILES !!! IT HAS SPECIAL MEANING!!!
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
! Constants
! Implicit functions
! External functions and subroutines
! Internal functions and subroutines
! Internal arrays
!===================================================================
!===================================================================
!
! Calculate dipole matrix elements for all wavefunctions available
! and the three cartesian directions. In bulk systems, the dipoles
! are calculated along the three primitive lattice directions.
!
! Dipole matrix elements are calculated between all possible pairs
! of occupied (iv) and unoccupied (ic) states.
! Dipole given in atomic units (Bohr radius = 1):
!
! dip(iv,ic,1) = < v | x | c >
! dip(iv,ic,2) = < v | y | c >
! dip(iv,ic,3) = < v | z | c >
!
! In periodic systems, calculates dipole matrix elements between
! occupied (i) and empty (j) orbitals using the velocity operator:
!
! dip_(i,j,:) = < i | v(:) | j > / (E_j - E_i)
!
! For non-periodic systems, the matrix elements dip_(i,j) are
! identical to matrix elements of the position operator (and
! hence dipole):
!
! dip_(i,j,:) = < i | r(:) | j >
!
! For periodic systems, the identity above does not hold because
! the orbitals are extended and matrix elements involving the
! position operator are ill defined. Corrections due to the non
! -local pseudo-potentials are included in the velocity operator:
!
! v = p/m + (i/hbar) * V_nl (r,r') * [r' - r]
!
! OUTPUT:
!    wfn%ndip : number of dipole matrix elements (usually the
!               product of number of unoccupied and occupied orbitals)
!    wfn%mapd : address of orbitals (iv,ic) for each dipole
!    wfn%zdipole : dipole matrix elements
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
subroutine zget_dipole(gvec,wfn,ik,kvec,wfn_occ)

  use typedefs
  use mpi_module
  use fft_module
  use fd_module
  use psp_module
  implicit none

  ! arguments
  type (gspace), intent(inout) :: gvec
  type (wavefunction), intent(inout) :: wfn
  ! index of current k-point
  integer, intent(in) :: ik
  ! coordinates of current k-point, in units of reciprocal lattice vectors
  real(dp), intent(in) :: kvec(3)
  ! occupancy of electronic orbitals
  real(dp), intent(in) :: wfn_occ(wfn%nstate)

  ! local variables
  character (len=800) :: lastwords
  logical :: use_velocity
  integer :: ii, jj, iv, ic, jv, jc, nv, nc, mv, mc, irp, gr(3), &
       occ_bnds(4), ngrid, off, ioff, joff, mygrid, jj_loc, icol_pe, ipe
  real(dp) :: vwr, etr
  complex(dpc) :: rr(3)
  ! Cartesian coordinates of k-vector
  real(dp) :: kcart(3)
  ! threshold in the energy denominator
  real(dp), parameter :: EN_DEG = 1.0d-3

  integer, allocatable :: chi_tmp(:)
  complex(dpc), allocatable :: wfn1(:), wfn2(:), wfn_loc(:), &
       wvec_loc(:,:), w_distr(:,:), wvec(:,:), dip(:,:,:)
  complex(dpc), external :: zdot_c

  call dmatvec3('N',gvec%bvec,kvec,kcart)

  occ_bnds = 0
  occ_bnds(1) = wfn%nstate
  do ii = 1, wfn%nstate
     if ( wfn%map(ii) == 0 ) cycle
     if ( ii < occ_bnds(1) ) occ_bnds(1) = ii
  enddo
  do ii = 1, wfn%nstate
     if ( wfn%map(ii) == 0 ) cycle
     if ( ii > occ_bnds(4) ) occ_bnds(4) = ii
  enddo
  do ii = 1, wfn%nstate
     if ( wfn%map(ii) == 0 ) cycle
     if (wfn_occ(ii) > tol_occ)  occ_bnds(2) = ii
  enddo
  do ii = wfn%nstate, 1, -1
     if ( wfn%map(ii) == 0 ) cycle
     if (wfn_occ(ii) < one - tol_occ) occ_bnds(3) = ii
  enddo

  if (gvec%per > 0) then
     use_velocity = .true.
  else
     use_velocity = .false.
  endif

  ngrid = w_grp%ldn * w_grp%npes * gvec%syms%ntrans
  off = w_grp%offset
  mygrid = w_grp%mydim
  nv = occ_bnds(2) - occ_bnds(1) + 1
  nc = occ_bnds(4) - occ_bnds(3) + 1
  if (nv < 1 .or. nc < 1) then
     wfn%ndip = 0
     return
  endif
  allocate(dip(nv,nc,3))
  dip = zzero
  !-------------------------------------------------------------------
  ! Allocate data.
  !
  allocate(wvec(3,ngrid),stat=ii)
  call alccheck('wvec','dipole',3*ngrid,ii)
  wvec = zzero
  allocate(wfn1(ngrid),stat=ii)
  call alccheck('wfn1','dipole',ngrid,ii)
  allocate(wfn2(w_grp%ldn*gvec%syms%ntrans),stat=ii)
  call alccheck('wfn2','dipole',w_grp%ldn*gvec%syms%ntrans,ii)
  wfn2 = zzero
  allocate(chi_tmp(gvec%syms%ntrans))
  if (use_velocity) then
     allocate(w_distr(w_grp%ldn,w_grp%npes),stat=ii)
     call alccheck('w_distr','dipole',w_grp%ldn*w_grp%npes,ii)
     w_distr = zzero
     allocate(wvec_loc(3,w_grp%nr * gvec%syms%ntrans),stat=ii)
     call alccheck('wvec_loc','dipole',3*w_grp%nr*gvec%syms%ntrans,ii)
     allocate(wfn_loc(w_grp%nr),stat=ii)
     call alccheck('wfn_loc','dipole',w_grp%nr,ii)
  else
     !
     ! Calculate wvec = (r1,r2,r3) in the real space grid.
     !
     do irp = 1, gvec%syms%ntrans
        do ii = 1, mygrid
           call unfold(gvec%r(1,ii+off),gvec%syms%trans(1,1,irp),gvec%shift,gr)
           rr = (real(gr,dp) + gvec%shift(:) )*gvec%step(:)
           jj = w_grp%ldn*(irp-1) + ii
           wvec(:,jj) = rr
        enddo
     enddo
  endif

  if (fd%norder < 0) call zinitialize_FFT(peinf%inode,fft_box)

  !-------------------------------------------------------------------
  ! Start calculation.
  !
  do icol_pe = 1, nc, peinf%npes
     if (use_velocity) then
        do ipe = 0, w_grp%npes - 1
           ic = icol_pe + ipe + &
                w_grp%npes * w_grp%mygr + r_grp%npes * r_grp%mygr
           if (ic > nc) cycle
           mc = ic + occ_bnds(3) - 1
           jc = wfn%map(mc)
           if (jc == 0) then
              write(lastwords,*) ' ERROR in dipole: could not find ', &
                   'wavefunction ',mc,' in memory! ',ic, jc
              call die(lastwords)
           endif
           call zcopy(mygrid,wfn%zwf(1,jc),1,w_distr(1,ipe+1),1)
        enddo
        call zgather(1,w_distr,wfn_loc)
     endif
     ic = icol_pe + w_grp%inode + &
          w_grp%npes * w_grp%mygr + r_grp%npes * r_grp%mygr
     if (ic <= nc) then
        mc = ic + occ_bnds(3) - 1
        jc = wfn%map(mc)
        if (jc == 0) then
           write(lastwords,*) ' ERROR in dipole: could not find ', &
                'wavefunction ', mc, ' in memory! ', ic, jc
           call die(lastwords)
        endif
        if (use_velocity) then
           irp = wfn%jrep(0,mc)
           if (fd%norder < 0) then
              call zget_grad_FFT(gvec,wfn_loc,wvec_loc,irp)
           else
              call zget_grad_fd(gvec%syms,wfn_loc,wvec_loc,irp)
           endif
        else
           do ipe = 0, w_grp%npes - 1
              ic = icol_pe + ipe + &
                   w_grp%npes * w_grp%mygr + r_grp%npes * r_grp%mygr
              if (ic > nc) cycle
              mc = ic + occ_bnds(3) - 1
              jc = wfn%map(mc)
              if (jc == 0) then
                 write(lastwords,*) ' ERROR in dipole: could not find ', &
                      'wavefunction ',mc,' in memory! ',ic, jc
                 call die(lastwords)
              endif
              do irp = 1, gvec%syms%ntrans
                 do ii = 1, mygrid
                    jj = w_grp%ldn*((irp-1)*w_grp%npes + ipe) + ii
                    wfn1(jj) = wfn%zwf(ii,jc) * real(gvec%syms%chi(wfn%jrep(0,mc),irp),dp)
                 enddo
              enddo
           enddo
        endif
     endif
     if (use_velocity) then
        do irp = 1, gvec%syms%ntrans
           ii = w_grp%ldn*w_grp%npes*(irp-1) + 1
           jj = w_grp%nr * ( irp - 1 ) + 1
           call zscatter(3,wvec(1,ii),wvec_loc(1,jj))
        enddo
     endif
     do ipe = 0, w_grp%npes - 1
        ic = icol_pe + ipe + w_grp%npes * w_grp%mygr + r_grp%npes * r_grp%mygr
        if (ic > nc) cycle
        do iv = 1, nv
           mv = iv + occ_bnds(1) - 1
           jv = wfn%map(mv)
           if (jv == 0) then
              write(lastwords,*) ' ERROR in dipole : orbital ', mv, &
                   ' not found ! STOP'
              call die(lastwords)
           endif
           irp = wfn%jrep(0,mv)
           chi_tmp = gvec%syms%chi(irp,:)
           do irp = 1, gvec%syms%ntrans
              do ii = 1, mygrid
                 jj = w_grp%ldn*(irp-1) + ii
                 if (use_velocity) then
                    jj_loc = w_grp%nr * ( irp - 1 ) + ii + off
                    wfn2(jj) =  chi_tmp(gvec%rtrans(jj_loc)) * &
                         wfn%zwf(gvec%rindex(jj_loc)-off,jv)
                 else
                    wfn2(jj) = wfn%zwf(ii,jv) * real(chi_tmp(irp),dp)
                 endif
                 wfn2(jj) = conjg(wfn2(jj))
              enddo
           enddo
           if (use_velocity) then
              !  dip(iv,ic,:)  --> < v | Grad/m + i k/m | c >
              do irp = 1, gvec%syms%ntrans
                 ioff = ((irp-1)*w_grp%npes + ipe)*w_grp%ldn + 1
                 joff = (irp-1)*w_grp%ldn + 1
                 call zgemv('N',3,mygrid,zone,wvec(1,ioff),3,wfn2(joff),1,zzero,rr,1)
                 dip(iv,ic,:) = dip(iv,ic,:) + rr * two
              enddo
              do ii = 1, gvec%per
                 dip(iv,ic,ii) = dip(iv,ic,ii) + &
                      zi * two * kcart(ii) * real(gvec%syms%ntrans,dp) * &
                      zdot_c(mygrid,wfn%zwf(1,jv),1,wfn%zwf(1,jc),1)
              enddo
           else
              !  dip(iv,ic,:)  --> < v | r | c >
              do irp = 1, gvec%syms%ntrans
                 ioff = ((irp-1)*w_grp%npes + ipe)*w_grp%ldn + 1
                 joff = (irp-1)*w_grp%ldn + 1
                 call zmultiply_vec(mygrid,wfn1(ioff),wfn2(joff))
                 ioff = (irp-1)*w_grp%ldn + 1
                 call zgemv('N',3,mygrid,zone,wvec(1,ioff),3,wfn2(joff),1,zzero,rr,1)
                 dip(iv,ic,:) = dip(iv,ic,:) + rr
              enddo
           endif
        enddo  !iv
     enddo  ! do ipe = 0, w_grp%npes-1
  enddo  !ic

  if (fd%norder < 0) call zfinalize_FFT(peinf%inode,fft_box)

  !-------------------------------------------------------------------
  ! In periodic systems, must add the energy denominator and the
  ! non-local correction.
  !
  if (use_velocity) then

     do ii = 1, type_num
        call znonloc(gvec,psp(ii),wfn,occ_bnds,ii,kcart,dip)
     enddo

     do ic = occ_bnds(3), occ_bnds(4)
        jc = ic - occ_bnds(3) + 1
        vwr = wfn%e1(ic)
        do iv = occ_bnds(1), occ_bnds(2)
           jv = iv - occ_bnds(1) + 1
           etr = vwr - wfn%e1(iv)
           if ( etr < EN_DEG ) then
              if (peinf%master) then
                 write(6,*) ' WARNING: states ',ic,iv,' are almost degenerate!'
                 write(6,*) 'Cannot calculate velocity matrix element. ', &
                      'Reset it to zero.'
              endif
              dip(jv,jc,:) = zzero
           else
              dip(jv,jc,:) = dip(jv,jc,:) / etr
           endif
        enddo
     enddo
  endif

  !-------------------------------------------------------------------
  ! Done. Deallocate memory and save data.
  !
  deallocate(wvec)
  deallocate(wfn1)
  deallocate(wfn2)
  deallocate(chi_tmp)
  if (use_velocity) then
     deallocate(wvec_loc)
     deallocate(wfn_loc)
     deallocate(w_distr)
  endif
  call zpsum(nv*nc*3,peinf%npes,peinf%comm,dip)

  wfn%ndip = nv*nc
  allocate(wfn%zdipole(wfn%ndip,3))
  allocate(wfn%mapd(4,wfn%ndip))
  ii = 0
  do ic = occ_bnds(3),occ_bnds(4)
     do iv = occ_bnds(1),occ_bnds(2)
        ii = ii + 1
        wfn%mapd(1,ii) = iv
        wfn%mapd(2,ii) = ic
        wfn%mapd(3,ii) = ik
        wfn%mapd(4,ii) = ik
        wfn%zdipole(ii,:) = dip(iv-occ_bnds(1)+1,ic-occ_bnds(3)+1,:)
     enddo
  enddo

  deallocate(dip)

end subroutine zget_dipole
!===============================================================
