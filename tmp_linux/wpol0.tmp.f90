












!===================================================================
!
! Macros for the complex pre-processing. All source files with extension
! .F90z are pre-processed twice. Once with CPLX and then again without CPLX.
! Most instances of capital "Z" are replaced by either lower case "z" or
! lower case "d" respectively with or without CPLX.
! BE CAREFUL WITH CAPITAL "Z" IN .F90z FILES !!! IT HAS SPECIAL MEANING!!!
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
! Constants
! Implicit functions
! External functions and subroutines
! Internal functions and subroutines
! Internal arrays
!===================================================================
!===================================================================
!
! Calculate wpol0(r), the static limit of the proper 
! screened interaction W_pol, as defined by:
!
!    W_pol(r,r';E) = W(r,r';E) - e^2/|r - r'| ,
!    wpol0(r) = W_pol(r,r'=r;E=0) .
!
! The static limit wpol0 can be used in a self-energy calculation
! in order to include the static remainder to Sigma_c (correlation
! part of self-energy), as an attempt to improve the convergence
! of Sigma_c with respect to the number of LDA orbitals included in
! the Green's function. If one defines Sc(n) as Sigma_c obtained
! by summing over the lowest n orbitals, then one can assume the
! approximate relation:
!
! Sc(infty) = Sc(n) + Delta,
!
! with Delta = Sc(infty) - Sc(n) evaluated in the static limit.
!
! In the static limit, Sc(infty) can be evaluated easily from
! wpol0(r), see e.g. Hybertsen & Louie PRB 34, 5390 (1986) 
! Eq. 17ab and 19ab.
!
! Generally speaking, the static remainder Delta is responsible
! for a decrease in all quasi-particle energies, which arises from
! the underestimate of the Coulomb-hole interaction when Sc(infty)
! is replaced by Sc(n) (see ref. above). This underestimate is
! less important if one looks at differences between quasi-particle
! energies, like the energy gap, than at the actual energies. More
! about the static remainder can be found in Appendix B of Tiago
! & Chelikowsky, PRB (2006).
!
! Structures pol(:) have the value of these elements modified:
!    nn : equal to k_p(:)%nn, old value may be 0 or otherwise
!         inconsistent with k_p(:)%nn (this happens if TDLDA
!         eigenvectors were not calculated in the same run).
!    tv  : read from disk.
!    ltv : flag that indicates v is allocated.
!
! At exit, pol(:)%tv is deallocated and pol(:)%ltv is set to false.
!
! All output of this subroutine is written to files wpol0.dat and
! wpol0_rho.dat.
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
subroutine dwpol0(gvec,kpt,qpt,pol,k_p,nolda,nrep,nspin,iq0,nr_buff)

  use typedefs
  use mpi_module
  implicit none
  include 'mpif.h'

  ! arguments
  ! real-space grid
  type (gspace), intent(inout) :: gvec
  ! k-points (from DFT) and q-vectors
  type (kptinfo), intent(in) :: kpt
  type (qptinfo), intent(in) :: qpt
  ! TDDFT polarizability
  type (polinfo), intent(inout) :: pol(nrep)
  ! TDDFT kernel
  type (kernelinfo), intent(in) :: k_p(nrep)
  ! true if LDA kernel is ignored
  logical, intent(in) :: nolda
  integer, intent(in) :: &
       nrep, &        ! number of representations
       nspin, &       ! number of spin channels
       iq0, &         ! index of current q-vector
       nr_buff        ! length of output buffer (defines at how many
                      ! points the static limit is calculated)

  ! local variables
  character (len=13) :: filnam
  logical :: alloc_fail
  integer :: ii, ig, info, npol, mpol, ncount, ipol, jpol, &
       ipe, jpe, istart, nr_pe, irp, jrp, rpt(3)
  real(dp) :: xsum
  real(dp), dimension(:), allocatable :: vr, vrsum
  real(dp), dimension(:,:), allocatable :: ver, vtrans, vevtrans, &
       wpolr, vvc, rdum1, rdum2, fr, frsum
  real(dp), dimension(:,:,:), allocatable :: fvc
  integer :: status(MPI_STATUS_SIZE)
  integer, parameter :: &
       pol_unit = 52, &          ! unit for pol_diag.dat file
       scratch = 63, &           ! unit for scratch data
       out_unit = 34, &          ! unit for wpol0.dat
       out2_unit = 35            ! unit for wpol0_rho.dat

  !-------------------------------------------------------------------
  ! Prepare scratch files.
  !
  write(filnam,'(a9,i4.4)') 'TMPMATEL_', peinf%inode
  open(scratch,file=filnam,form='unformatted')
  rewind(scratch)

  call MPI_BARRIER(peinf%comm,info)
  !-------------------------------------------------------------------
  ! Calculate wpol=X in real space and write it on scratch files.
  !
  do jrp = 1, nrep/r_grp%num
     irp = r_grp%g_rep(jrp)
     pol(irp)%nn = k_p(irp)%nn
     if ( pol(irp)%ntr == 0 ) cycle
     call dwpol_v(gvec,kpt,pol(irp),nolda,irp,nspin,nr_buff,scratch,qpt%fk(1,iq0))
  enddo

  rewind(scratch)

  call MPI_BARRIER(peinf%comm,info)
  !-------------------------------------------------------------------
  ! Try different grid sizes until all arrays fit in memory
  ! allocate one eigenvector array as well.
  !
  mpol = maxval( pol(:)%nn )
  npol = maxval( pol(:)%ntr )
  allocate(pol(1)%dv(mpol*r_grp%npes,mpol))

  alloc_fail = .true.
  ! Start with a grid size that fits buffer space.
  nr_pe = nr_buff / r_grp%npes
  do while (alloc_fail)
     allocate(fvc(npol,2*nr_pe,nspin+1),stat=info)
     call MPI_ALLREDUCE(info,ii,1,MPI_INTEGER,MPI_SUM,peinf%comm,info)
     info = ii
     if (allocated(fvc)) deallocate(fvc)
     if ( info == 0 )  then
        alloc_fail= .false.
     else
        ! Reduce array sizes by 100 MB or more.
        ncount = max(6553600/npol/nr_pe,100)
        nr_pe = nr_pe - ncount
     endif
  enddo
  ! Make sure all PEs have the same number of grid points.
  call MPI_ALLREDUCE(nr_pe,ii,1,MPI_INTEGER,MPI_MIN,peinf%comm,info)
  nr_pe = ii
  deallocate(pol(1)%dv)
  !-------------------------------------------------------------------
  ! Reopen file with polarizability eigenstates and prepare to
  ! calculate Polarizability operator.
  !
  if (r_grp%master) then
     write(6,*) ' Distributing grid points among PEs. '
     write(6,*) nr_pe,' grid points per PE. '
     open(pol_unit,file='pol_diag.dat',form='unformatted')
     rewind(pol_unit)
     read(pol_unit)
     read(pol_unit)
  endif

  allocate(vr(nr_buff))
  vr = zero
  allocate(fr(nr_buff,nspin))
  fr = zero
  allocate(vrsum(nr_buff))
  vrsum = zero
  allocate(frsum(nr_buff,nspin))
  frsum = zero
  !-------------------------------------------------------------------
  ! Calculate Polarizability operator:
  ! W = conjg(X) * (E_tdlda ) * X
  ! where E_tdlda, X are eigenvalues/eigenvectors of the TDLDA problem.
  ! W is a global matrix.
  !
  ! For each representation, read the corresponding X on disk.
  !
  do jrp = 1, nrep/r_grp%num
     irp = r_grp%g_rep(jrp)
     if ( pol(irp)%ntr == 0 ) cycle

     call dget_pol(pol(irp),pol_unit,nrep,qpt%nk,irp,iq0,k_p(irp)%nn)

     npol = pol(irp)%nn * r_grp%npes
     mpol = pol(irp)%nn

     allocate(wpolr(npol,mpol),stat=info)
     call alccheck('wpolr','wpol0',npol*mpol,info)

     allocate(ver(mpol,mpol),stat=info)
     call alccheck('ver','wpol0',mpol*mpol,info)
     allocate(vtrans(mpol,mpol),stat=info)
     call alccheck('vtrans','wpol0',mpol*mpol,info)
     allocate(vevtrans(mpol,mpol),stat=info)
     call alccheck('vevtrans','wpol0',mpol*mpol,info)

     ! W. Gao: calculate:
     !   A_{vc,v'c'}=\sum_s X^s_vc sqrt(e_c-e_v/omega_s) X^s_v'c' sqrt(e_c'-e_v'/omega_s)/omega_s
     !   A_{vc,v'c'} is stored in wpolr
     ! each processor in the r_grp stores npol rows and mpol columns
     do ipe = 0, r_grp%npes - 1

        ver = zero
        do jpol = 1, mpol
           ipol = jpol + r_grp%inode*mpol
           if (ipol > pol(irp)%ntr) exit
           istart = 1 + ipe * mpol
           xsum = one / pol(irp)%eig(ipol)
           call dcopy(mpol,pol(irp)%dtv(jpol,istart),mpol,ver(1,jpol),1)
           call dscal(mpol,xsum,ver(1,jpol),1)
        enddo
        ver = (ver)

        do jpe = 0, r_grp%npes - 1
           vtrans = zero
           do jpol = 1, mpol
              ipol = jpol + jpe * mpol
              call dcopy(mpol,pol(irp)%dtv(1,ipol),1,vtrans(1,jpol),1)
           enddo

           call dgemm('n','n',mpol,mpol,mpol,one,ver,mpol, &
                vtrans,mpol,zero,vevtrans,mpol)
           call dpsum(mpol*mpol,r_grp%npes,r_grp%comm,vevtrans)
           if (r_grp%inode == jpe) then
              do jpol = 1, mpol
                 ipol = jpol + ipe * mpol
                 call dcopy(mpol,vevtrans(jpol,1),mpol,wpolr(ipol,1),npol)
              enddo
           endif
        enddo
     enddo

     deallocate(ver, vtrans, vevtrans)
     if (pol(irp)%ltv) then
        deallocate(pol(irp)%dtv)
        pol(irp)%ltv = .false.
     endif

     npol = pol(irp)%ntr
     allocate(fvc(npol,nr_pe,nspin),stat=info)
     call alccheck('fvc','wpol0',npol*nr_pe*nspin,info)
     fvc = zero
     allocate(vvc(npol,nr_pe),stat=info)
     call alccheck('vvc','wpol0',npol*nr_pe,info)
     vvc = zero

     ! each processor of r_grp store part of the grid points
     istart = 1 + nr_pe*r_grp%inode ! inode keep the data starting from istart
     ncount = nr_pe*(r_grp%inode + 1)
     if (ncount > nr_buff) ncount = nr_buff
     ncount = ncount - istart + 1

     ! read v_vc and f_vc from TMPMATEL_**
     do ipol = 1, npol
        ipe = mod(ipol-1,r_grp%npes)
        if ( r_grp%inode == ipe ) read(scratch) &
             (vr(ig),ig=1,nr_buff),((fr(ig,ii),ig=1,nr_buff),ii=1,nspin)
        call MPI_BCAST(vr, nr_buff, MPI_DOUBLE_PRECISION, &
             ipe,r_grp%comm,info)
        call MPI_BCAST(fr, nr_buff*nspin, MPI_DOUBLE_PRECISION, &
             ipe,r_grp%comm,info)
        if (ncount > 0) then
           call dcopy(ncount,vr(istart),1,vvc(ipol,1),npol)
           call dcopy(ncount,fr(istart,1),1,fvc(ipol,1,1),npol)
           if (nspin == 2) &
                call dcopy(ncount,fr(istart,2),1,fvc(ipol,1,2),npol)
        endif
        call MPI_BARRIER(r_grp%comm,info)
     enddo

     vr = zero
     fr = zero
     allocate(rdum1(pol(irp)%nn * r_grp%npes,1+nspin))
     allocate(rdum2(pol(irp)%nn,1+nspin))
     
     ! pol(irp)%nn * rgrp%npes >= pol%ntr, pol%ntr is the number of cv pairs??
     do ipe = 0, r_grp%npes - 1
        do ig = 1, nr_pe
           rdum1 = zero
           rdum2 = zero

           ! copy vvc, fvc to rdum1, why do we do this?? is this efficient?
           ! rdum1(1:npol,1) = vvc(1:npol,ig)  npol is the number of (v,c) pairs
           ! rdum1(1:npol,2) = fvc(1:npol,ig,1)
           ! rdum1(1:npol,3) = fvc(1:npol,ig,2)
           if (ipe == r_grp%inode) then
              call dcopy(npol,vvc(1,ig),1,rdum1(1,1),1)
              call dcopy(npol,fvc(1,ig,1),1,rdum1(1,2),1)
              if (nspin == 2) call dcopy(npol,fvc(1,ig,2),1,rdum1(1,3),1)
           endif
           call MPI_BCAST(rdum1, pol(irp)%nn * r_grp%npes * (1+nspin), &
                MPI_DOUBLE_PRECISION,ipe,r_grp%comm,info)
           istart = 1 + r_grp%inode * pol(irp)%nn
           ncount = (1 + r_grp%inode) * pol(irp)%nn
           if ( ncount > npol ) ncount = npol
           ncount = ncount - istart + 1
           ! copy part of rdum to rdum2
           ! copy rdum1(istart:istart+ncount,1:3) to rdum2(istart:istart+ncount,1:3)
           ! rdum2(istart:istart+ncount,1:3) = vvc(istart:istart+ncount), and fvc(istart:istart+ncount,1:2)
           do ii = 1, 1 + nspin
              if (ncount > 0) &
                   call dcopy(ncount,rdum1(istart,ii),1,rdum2(1,ii),1)
           enddo

           xsum = dot_product(rdum1(:,1),matmul(wpolr,rdum2(:,1)))
           call dpsum(1,r_grp%npes,r_grp%comm,xsum)
           if (ipe == r_grp%inode) vr(ig) = -two*xsum
           xsum = dot_product(rdum1(:,1),matmul(wpolr,rdum2(:,2)))
           call dpsum(1,r_grp%npes,r_grp%comm,xsum)
           if (ipe == r_grp%inode) fr(ig,1) = -two*xsum
           if (nspin == 2) then
              xsum = dot_product(rdum1(:,1),matmul(wpolr,rdum2(:,3)))
              call dpsum(1,r_grp%npes,r_grp%comm,xsum)
              if (ipe == r_grp%inode) fr(ig,2) = -two*xsum
           endif
           if (r_grp%master .and. (mod(ig,max(nr_pe/5,1)) == 0) ) then
              call stopwatch(.true.,' vr/fr calculation')
              write(6,'(i10,a,i10,a,i2,a,i4)') ig, ' out of ', nr_pe, &
                   ' representation ', irp, ' processor ', ipe
           endif
        enddo ! ig 
     enddo ! ipe

     deallocate(rdum1)
     deallocate(rdum2)

     if (r_grp%master) then
        istart = nr_pe*r_grp%inode
        ncount = nr_pe*r_grp%inode + nr_pe
        if (ncount > nr_buff) ncount = nr_buff
        ncount = ncount - istart
        call daxpy(ncount,one,vr,1,vrsum(1+istart),1)
        call daxpy(ncount,one,fr(1,1),1,frsum(1,1+istart),1)
        if (nspin == 2) &
             call daxpy(ncount,one,fr(1,2),1,frsum(1+istart,2),1)
     endif

     ! The master node in each r_grp collect all the data in vrsum(1:nr_buff)
     do ipe = 0, r_grp%npes - 1
        if (.not. r_grp%master) then
           if ( ipe == r_grp%inode ) then
              call MPI_SEND(vr,nr_pe,MPI_DOUBLE_PRECISION, &
                   r_grp%masterid,r_grp%inode,r_grp%comm,info)
              call MPI_SEND(fr(1,1),nr_pe,MPI_DOUBLE_PRECISION, &
                   r_grp%masterid,r_grp%inode,r_grp%comm,info)
              if (nspin == 2) & 
                   call MPI_SEND(fr(1,2),nr_pe,MPI_DOUBLE_PRECISION, &
                   r_grp%masterid,r_grp%inode,r_grp%comm,info)
           endif
        else
           if ( ipe /= r_grp%inode) then
              call MPI_RECV(vr,nr_pe,MPI_DOUBLE_PRECISION, &
                   ipe,ipe,r_grp%comm,status,info)
              call MPI_RECV(fr(1,1),nr_pe,MPI_DOUBLE_PRECISION, &
                   ipe,ipe,r_grp%comm,status,info)
              if (nspin == 2) & 
                   call MPI_RECV(fr(1,2),nr_pe,MPI_DOUBLE_PRECISION, &
                   ipe,ipe,r_grp%comm,status,info)
              istart = nr_pe*ipe
              ncount = nr_pe*ipe + nr_pe
              if (ncount > nr_buff) ncount = nr_buff
              ncount = ncount - istart
              call daxpy(ncount,one,vr,1,vrsum(1+istart),1)
              call daxpy(ncount,one,fr(1,1),1,frsum(1+istart,1),1)
              if (nspin == 2) &
                   call daxpy(ncount,one,fr(1,2),1,frsum(1+istart,2),1)
           endif
        endif
        call MPI_BARRIER(r_grp%comm,info)
     enddo

     deallocate(vvc, fvc)
     deallocate(wpolr)
  enddo ! jrp = 1, nrep/r_grp%num

  ! Only the master node in each r_grp call psum subroutine
  if (r_grp%master) then
     close(pol_unit)
     call dpsum(nr_buff,r_grp%num,r_grp%m_comm,vrsum)
     call dpsum(nr_buff*nspin,r_grp%num,r_grp%m_comm,frsum)
  endif

  vrsum = vrsum / real(kpt%nk,dp)
  frsum = frsum / real(kpt%nk,dp)

  !-------------------------------------------------------------------
  ! Done. Delete scratch files and print files wpol0.dat and wpol0_rho.dat.
  !
  close(scratch, STATUS='DELETE')

  if (peinf%master) then
     vrsum = vrsum/real(nspin,dp)
     frsum = frsum/real(nspin,dp)
     open(out_unit,file='wpol0.dat',form='formatted')
     write(out_unit,*) gvec%syms%ntrans*nr_pe*r_grp%npes,gvec%nr,nspin
     open(out2_unit,file='wpol0_rho.dat',form='formatted')
     do ig = 1, nr_pe * r_grp%npes
        do irp = 1, gvec%syms%ntrans
           call unfold(gvec%r(1,ig),gvec%syms%trans(1,1,irp),gvec%shift,rpt)
           write(out_unit,'(3i5,3g15.6)') (rpt(ii),ii=1,3),vrsum(ig), &
                (frsum(ig,ii),ii=1,nspin)
        enddo
        xsum = one*( kpt%rho(ig,1) + kpt%rho(ig,nspin) )*real(nspin,dp)/two
        write(out2_unit,'(4g15.6)') xsum, vrsum(ig), (frsum(ig,ii),ii=1,nspin)
     enddo
     write(out_unit,*) gvec%step
     write(out_unit,*) gvec%shift
     close(out_unit)
     close(out2_unit)
  endif

  deallocate(fr, vr)
  deallocate(frsum, vrsum)

end subroutine dwpol0
!===================================================================













!===================================================================
!
! Macros for the complex pre-processing. All source files with extension
! .F90z are pre-processed twice. Once with 1 and then again without 1.
! Most instances of capital "Z" are replaced by either lower case "z" or
! lower case "d" respectively with or without 1.
! BE CAREFUL WITH CAPITAL "Z" IN .F90z FILES !!! IT HAS SPECIAL MEANING!!!
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
! Constants
! Implicit functions
! External functions and subroutines
! Internal functions and subroutines
! Internal arrays
!===================================================================
!===================================================================
!
! Calculate wpol0(r), the static limit of the proper 
! screened interaction W_pol, as defined by:
!
!    W_pol(r,r';E) = W(r,r';E) - e^2/|r - r'| ,
!    wpol0(r) = W_pol(r,r'=r;E=0) .
!
! The static limit wpol0 can be used in a self-energy calculation
! in order to include the static remainder to Sigma_c (correlation
! part of self-energy), as an attempt to improve the convergence
! of Sigma_c with respect to the number of LDA orbitals included in
! the Green's function. If one defines Sc(n) as Sigma_c obtained
! by summing over the lowest n orbitals, then one can assume the
! approximate relation:
!
! Sc(infty) = Sc(n) + Delta,
!
! with Delta = Sc(infty) - Sc(n) evaluated in the static limit.
!
! In the static limit, Sc(infty) can be evaluated easily from
! wpol0(r), see e.g. Hybertsen & Louie PRB 34, 5390 (1986) 
! Eq. 17ab and 19ab.
!
! Generally speaking, the static remainder Delta is responsible
! for a decrease in all quasi-particle energies, which arises from
! the underestimate of the Coulomb-hole interaction when Sc(infty)
! is replaced by Sc(n) (see ref. above). This underestimate is
! less important if one looks at differences between quasi-particle
! energies, like the energy gap, than at the actual energies. More
! about the static remainder can be found in Appendix B of Tiago
! & Chelikowsky, PRB (2006).
!
! Structures pol(:) have the value of these elements modified:
!    nn : equal to k_p(:)%nn, old value may be 0 or otherwise
!         inconsistent with k_p(:)%nn (this happens if TDLDA
!         eigenvectors were not calculated in the same run).
!    tv  : read from disk.
!    ltv : flag that indicates v is allocated.
!
! At exit, pol(:)%tv is deallocated and pol(:)%ltv is set to false.
!
! All output of this subroutine is written to files wpol0.dat and
! wpol0_rho.dat.
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
subroutine zwpol0(gvec,kpt,qpt,pol,k_p,nolda,nrep,nspin,iq0,nr_buff)

  use typedefs
  use mpi_module
  implicit none
  include 'mpif.h'

  ! arguments
  ! real-space grid
  type (gspace), intent(inout) :: gvec
  ! k-points (from DFT) and q-vectors
  type (kptinfo), intent(in) :: kpt
  type (qptinfo), intent(in) :: qpt
  ! TDDFT polarizability
  type (polinfo), intent(inout) :: pol(nrep)
  ! TDDFT kernel
  type (kernelinfo), intent(in) :: k_p(nrep)
  ! true if LDA kernel is ignored
  logical, intent(in) :: nolda
  integer, intent(in) :: &
       nrep, &        ! number of representations
       nspin, &       ! number of spin channels
       iq0, &         ! index of current q-vector
       nr_buff        ! length of output buffer (defines at how many
                      ! points the static limit is calculated)

  ! local variables
  character (len=13) :: filnam
  logical :: alloc_fail
  integer :: ii, ig, info, npol, mpol, ncount, ipol, jpol, &
       ipe, jpe, istart, nr_pe, irp, jrp, rpt(3)
  complex(dpc) :: xsum
  complex(dpc), dimension(:), allocatable :: vr, vrsum
  complex(dpc), dimension(:,:), allocatable :: ver, vtrans, vevtrans, &
       wpolr, vvc, rdum1, rdum2, fr, frsum
  complex(dpc), dimension(:,:,:), allocatable :: fvc
  integer :: status(MPI_STATUS_SIZE)
  integer, parameter :: &
       pol_unit = 52, &          ! unit for pol_diag.dat file
       scratch = 63, &           ! unit for scratch data
       out_unit = 34, &          ! unit for wpol0.dat
       out2_unit = 35            ! unit for wpol0_rho.dat

  !-------------------------------------------------------------------
  ! Prepare scratch files.
  !
  write(filnam,'(a9,i4.4)') 'TMPMATEL_', peinf%inode
  open(scratch,file=filnam,form='unformatted')
  rewind(scratch)

  call MPI_BARRIER(peinf%comm,info)
  !-------------------------------------------------------------------
  ! Calculate wpol=X in real space and write it on scratch files.
  !
  do jrp = 1, nrep/r_grp%num
     irp = r_grp%g_rep(jrp)
     pol(irp)%nn = k_p(irp)%nn
     if ( pol(irp)%ntr == 0 ) cycle
     call zwpol_v(gvec,kpt,pol(irp),nolda,irp,nspin,nr_buff,scratch,qpt%fk(1,iq0))
  enddo

  rewind(scratch)

  call MPI_BARRIER(peinf%comm,info)
  !-------------------------------------------------------------------
  ! Try different grid sizes until all arrays fit in memory
  ! allocate one eigenvector array as well.
  !
  mpol = maxval( pol(:)%nn )
  npol = maxval( pol(:)%ntr )
  allocate(pol(1)%zv(mpol*r_grp%npes,mpol))

  alloc_fail = .true.
  ! Start with a grid size that fits buffer space.
  nr_pe = nr_buff / r_grp%npes
  do while (alloc_fail)
     allocate(fvc(npol,2*nr_pe,nspin+1),stat=info)
     call MPI_ALLREDUCE(info,ii,1,MPI_INTEGER,MPI_SUM,peinf%comm,info)
     info = ii
     if (allocated(fvc)) deallocate(fvc)
     if ( info == 0 )  then
        alloc_fail= .false.
     else
        ! Reduce array sizes by 100 MB or more.
        ncount = max(6553600/npol/nr_pe,100)
        ncount = ncount / 2
        nr_pe = nr_pe - ncount
     endif
  enddo
  ! Make sure all PEs have the same number of grid points.
  call MPI_ALLREDUCE(nr_pe,ii,1,MPI_INTEGER,MPI_MIN,peinf%comm,info)
  nr_pe = ii
  deallocate(pol(1)%zv)
  !-------------------------------------------------------------------
  ! Reopen file with polarizability eigenstates and prepare to
  ! calculate Polarizability operator.
  !
  if (r_grp%master) then
     write(6,*) ' Distributing grid points among PEs. '
     write(6,*) nr_pe,' grid points per PE. '
     open(pol_unit,file='pol_diag.dat',form='unformatted')
     rewind(pol_unit)
     read(pol_unit)
     read(pol_unit)
  endif

  allocate(vr(nr_buff))
  vr = zzero
  allocate(fr(nr_buff,nspin))
  fr = zzero
  allocate(vrsum(nr_buff))
  vrsum = zzero
  allocate(frsum(nr_buff,nspin))
  frsum = zzero
  !-------------------------------------------------------------------
  ! Calculate Polarizability operator:
  ! W = conjg(X) * (E_tdlda ) * X
  ! where E_tdlda, X are eigenvalues/eigenvectors of the TDLDA problem.
  ! W is a global matrix.
  !
  ! For each representation, read the corresponding X on disk.
  !
  do jrp = 1, nrep/r_grp%num
     irp = r_grp%g_rep(jrp)
     if ( pol(irp)%ntr == 0 ) cycle

     call zget_pol(pol(irp),pol_unit,nrep,qpt%nk,irp,iq0,k_p(irp)%nn)

     npol = pol(irp)%nn * r_grp%npes
     mpol = pol(irp)%nn

     allocate(wpolr(npol,mpol),stat=info)
     call alccheck('wpolr','wpol0',npol*mpol,info)

     allocate(ver(mpol,mpol),stat=info)
     call alccheck('ver','wpol0',mpol*mpol,info)
     allocate(vtrans(mpol,mpol),stat=info)
     call alccheck('vtrans','wpol0',mpol*mpol,info)
     allocate(vevtrans(mpol,mpol),stat=info)
     call alccheck('vevtrans','wpol0',mpol*mpol,info)

     ! W. Gao: calculate:
     !   A_{vc,v'c'}=\sum_s X^s_vc sqrt(e_c-e_v/omega_s) X^s_v'c' sqrt(e_c'-e_v'/omega_s)/omega_s
     !   A_{vc,v'c'} is stored in wpolr
     ! each processor in the r_grp stores npol rows and mpol columns
     do ipe = 0, r_grp%npes - 1

        ver = zzero
        do jpol = 1, mpol
           ipol = jpol + r_grp%inode*mpol
           if (ipol > pol(irp)%ntr) exit
           istart = 1 + ipe * mpol
           xsum = zone / pol(irp)%eig(ipol)
           call zcopy(mpol,pol(irp)%ztv(jpol,istart),mpol,ver(1,jpol),1)
           call zscal(mpol,xsum,ver(1,jpol),1)
        enddo
        ver = conjg(ver)

        do jpe = 0, r_grp%npes - 1
           vtrans = zzero
           do jpol = 1, mpol
              ipol = jpol + jpe * mpol
              call zcopy(mpol,pol(irp)%ztv(1,ipol),1,vtrans(1,jpol),1)
           enddo

           call zgemm('n','n',mpol,mpol,mpol,zone,ver,mpol, &
                vtrans,mpol,zzero,vevtrans,mpol)
           call zpsum(mpol*mpol,r_grp%npes,r_grp%comm,vevtrans)
           if (r_grp%inode == jpe) then
              do jpol = 1, mpol
                 ipol = jpol + ipe * mpol
                 call zcopy(mpol,vevtrans(jpol,1),mpol,wpolr(ipol,1),npol)
              enddo
           endif
        enddo
     enddo

     deallocate(ver, vtrans, vevtrans)
     if (pol(irp)%ltv) then
        deallocate(pol(irp)%ztv)
        pol(irp)%ltv = .false.
     endif

     npol = pol(irp)%ntr
     allocate(fvc(npol,nr_pe,nspin),stat=info)
     call alccheck('fvc','wpol0',npol*nr_pe*nspin,info)
     fvc = zzero
     allocate(vvc(npol,nr_pe),stat=info)
     call alccheck('vvc','wpol0',npol*nr_pe,info)
     vvc = zzero

     ! each processor of r_grp store part of the grid points
     istart = 1 + nr_pe*r_grp%inode ! inode keep the data starting from istart
     ncount = nr_pe*(r_grp%inode + 1)
     if (ncount > nr_buff) ncount = nr_buff
     ncount = ncount - istart + 1

     ! read v_vc and f_vc from TMPMATEL_**
     do ipol = 1, npol
        ipe = mod(ipol-1,r_grp%npes)
        if ( r_grp%inode == ipe ) read(scratch) &
             (vr(ig),ig=1,nr_buff),((fr(ig,ii),ig=1,nr_buff),ii=1,nspin)
        call MPI_BCAST(vr, nr_buff, MPI_DOUBLE_COMPLEX, &
             ipe,r_grp%comm,info)
        call MPI_BCAST(fr, nr_buff*nspin, MPI_DOUBLE_COMPLEX, &
             ipe,r_grp%comm,info)
        if (ncount > 0) then
           call zcopy(ncount,vr(istart),1,vvc(ipol,1),npol)
           call zcopy(ncount,fr(istart,1),1,fvc(ipol,1,1),npol)
           if (nspin == 2) &
                call zcopy(ncount,fr(istart,2),1,fvc(ipol,1,2),npol)
        endif
        call MPI_BARRIER(r_grp%comm,info)
     enddo

     vr = zzero
     fr = zzero
     allocate(rdum1(pol(irp)%nn * r_grp%npes,1+nspin))
     allocate(rdum2(pol(irp)%nn,1+nspin))
     
     ! pol(irp)%nn * rgrp%npes >= pol%ntr, pol%ntr is the number of cv pairs??
     do ipe = 0, r_grp%npes - 1
        do ig = 1, nr_pe
           rdum1 = zzero
           rdum2 = zzero

           ! copy vvc, fvc to rdum1, why do we do this?? is this efficient?
           ! rdum1(1:npol,1) = vvc(1:npol,ig)  npol is the number of (v,c) pairs
           ! rdum1(1:npol,2) = fvc(1:npol,ig,1)
           ! rdum1(1:npol,3) = fvc(1:npol,ig,2)
           if (ipe == r_grp%inode) then
              call zcopy(npol,vvc(1,ig),1,rdum1(1,1),1)
              call zcopy(npol,fvc(1,ig,1),1,rdum1(1,2),1)
              if (nspin == 2) call zcopy(npol,fvc(1,ig,2),1,rdum1(1,3),1)
           endif
           call MPI_BCAST(rdum1, pol(irp)%nn * r_grp%npes * (1+nspin), &
                MPI_DOUBLE_COMPLEX,ipe,r_grp%comm,info)
           istart = 1 + r_grp%inode * pol(irp)%nn
           ncount = (1 + r_grp%inode) * pol(irp)%nn
           if ( ncount > npol ) ncount = npol
           ncount = ncount - istart + 1
           ! copy part of rdum to rdum2
           ! copy rdum1(istart:istart+ncount,1:3) to rdum2(istart:istart+ncount,1:3)
           ! rdum2(istart:istart+ncount,1:3) = vvc(istart:istart+ncount), and fvc(istart:istart+ncount,1:2)
           do ii = 1, 1 + nspin
              if (ncount > 0) &
                   call zcopy(ncount,rdum1(istart,ii),1,rdum2(1,ii),1)
           enddo

           xsum = dot_product(rdum1(:,1),matmul(wpolr,rdum2(:,1)))
           call zpsum(1,r_grp%npes,r_grp%comm,xsum)
           if (ipe == r_grp%inode) vr(ig) = -two*xsum
           xsum = dot_product(rdum1(:,1),matmul(wpolr,rdum2(:,2)))
           call zpsum(1,r_grp%npes,r_grp%comm,xsum)
           if (ipe == r_grp%inode) fr(ig,1) = -two*xsum
           if (nspin == 2) then
              xsum = dot_product(rdum1(:,1),matmul(wpolr,rdum2(:,3)))
              call zpsum(1,r_grp%npes,r_grp%comm,xsum)
              if (ipe == r_grp%inode) fr(ig,2) = -two*xsum
           endif
           if (r_grp%master .and. (mod(ig,max(nr_pe/5,1)) == 0) ) then
              call stopwatch(.true.,' vr/fr calculation')
              write(6,'(i10,a,i10,a,i2,a,i4)') ig, ' out of ', nr_pe, &
                   ' representation ', irp, ' processor ', ipe
           endif
        enddo ! ig 
     enddo ! ipe

     deallocate(rdum1)
     deallocate(rdum2)

     if (r_grp%master) then
        istart = nr_pe*r_grp%inode
        ncount = nr_pe*r_grp%inode + nr_pe
        if (ncount > nr_buff) ncount = nr_buff
        ncount = ncount - istart
        call zaxpy(ncount,zone,vr,1,vrsum(1+istart),1)
        call zaxpy(ncount,zone,fr(1,1),1,frsum(1,1+istart),1)
        if (nspin == 2) &
             call zaxpy(ncount,zone,fr(1,2),1,frsum(1+istart,2),1)
     endif

     ! The master node in each r_grp collect all the data in vrsum(1:nr_buff)
     do ipe = 0, r_grp%npes - 1
        if (.not. r_grp%master) then
           if ( ipe == r_grp%inode ) then
              call MPI_SEND(vr,nr_pe,MPI_DOUBLE_COMPLEX, &
                   r_grp%masterid,r_grp%inode,r_grp%comm,info)
              call MPI_SEND(fr(1,1),nr_pe,MPI_DOUBLE_COMPLEX, &
                   r_grp%masterid,r_grp%inode,r_grp%comm,info)
              if (nspin == 2) & 
                   call MPI_SEND(fr(1,2),nr_pe,MPI_DOUBLE_COMPLEX, &
                   r_grp%masterid,r_grp%inode,r_grp%comm,info)
           endif
        else
           if ( ipe /= r_grp%inode) then
              call MPI_RECV(vr,nr_pe,MPI_DOUBLE_COMPLEX, &
                   ipe,ipe,r_grp%comm,status,info)
              call MPI_RECV(fr(1,1),nr_pe,MPI_DOUBLE_COMPLEX, &
                   ipe,ipe,r_grp%comm,status,info)
              if (nspin == 2) & 
                   call MPI_RECV(fr(1,2),nr_pe,MPI_DOUBLE_COMPLEX, &
                   ipe,ipe,r_grp%comm,status,info)
              istart = nr_pe*ipe
              ncount = nr_pe*ipe + nr_pe
              if (ncount > nr_buff) ncount = nr_buff
              ncount = ncount - istart
              call zaxpy(ncount,zone,vr,1,vrsum(1+istart),1)
              call zaxpy(ncount,zone,fr(1,1),1,frsum(1+istart,1),1)
              if (nspin == 2) &
                   call zaxpy(ncount,zone,fr(1,2),1,frsum(1+istart,2),1)
           endif
        endif
        call MPI_BARRIER(r_grp%comm,info)
     enddo

     deallocate(vvc, fvc)
     deallocate(wpolr)
  enddo ! jrp = 1, nrep/r_grp%num

  ! Only the master node in each r_grp call psum subroutine
  if (r_grp%master) then
     close(pol_unit)
     call zpsum(nr_buff,r_grp%num,r_grp%m_comm,vrsum)
     call zpsum(nr_buff*nspin,r_grp%num,r_grp%m_comm,frsum)
  endif

  vrsum = vrsum / real(kpt%nk,dp)
  frsum = frsum / real(kpt%nk,dp)

  !-------------------------------------------------------------------
  ! Done. Delete scratch files and print files wpol0.dat and wpol0_rho.dat.
  !
  close(scratch, STATUS='DELETE')

  if (peinf%master) then
     vrsum = vrsum/real(nspin,dp)
     frsum = frsum/real(nspin,dp)
     open(out_unit,file='wpol0.dat',form='formatted')
     write(out_unit,*) gvec%syms%ntrans*nr_pe*r_grp%npes,gvec%nr,nspin
     open(out2_unit,file='wpol0_rho.dat',form='formatted')
     do ig = 1, nr_pe * r_grp%npes
        do irp = 1, gvec%syms%ntrans
           call unfold(gvec%r(1,ig),gvec%syms%trans(1,1,irp),gvec%shift,rpt)
           write(out_unit,'(3i5,6g15.6)') (rpt(ii),ii=1,3), &
                real(vrsum(ig),dp), aimag(vrsum(ig)), &
                (real(frsum(ig,ii),dp), aimag(frsum(ig,ii)),ii=1,nspin)
        enddo
        xsum = zone*( kpt%rho(ig,1) + kpt%rho(ig,nspin) )*real(nspin,dp)/two
        write(out2_unit,'(7g15.6)') real(xsum,dp), real(vrsum(ig),dp), aimag(vrsum(ig)), &
             (real(frsum(ig,ii),dp), aimag(frsum(ig,ii)),ii=1,nspin)
     enddo
     write(out_unit,*) gvec%step
     write(out_unit,*) gvec%shift
     close(out_unit)
     close(out2_unit)
  endif

  deallocate(fr, vr)
  deallocate(frsum, vrsum)

end subroutine zwpol0
!===================================================================
