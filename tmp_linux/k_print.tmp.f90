












!===================================================================
!
! Macros for the complex pre-processing. All source files with extension
! .F90z are pre-processed twice. Once with CPLX and then again without CPLX.
! Most instances of capital "Z" are replaced by either lower case "z" or
! lower case "d" respectively with or without CPLX.
! BE CAREFUL WITH CAPITAL "Z" IN .F90z FILES !!! IT HAS SPECIAL MEANING!!!
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
! Constants
! Implicit functions
! External functions and subroutines
! Internal functions and subroutines
! Internal arrays
!===================================================================
!===================================================================
!
! Printing routine. Prints kernel matrix elements in fort.* files.
! Useful only in debug mode.
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
subroutine dk_print(inode,m1,m2,m3,m4,k1,k2,k3,k4,rsp,csp,matel)

  use myconstants
  implicit none
  integer, intent(in) :: inode, m1, m2, m3, m4, k1, k2, k3, k4, rsp, csp
  real(dp), intent(in) :: matel

  integer, parameter :: ntape = 84
  integer :: nn

  nn = ntape + inode
  write(nn,'(4i6,6i3,2g20.10)') m1, m2, m3, m4, k1, k2, k3, k4, rsp, csp, &
       matel

end subroutine dk_print
!===================================================================













!===================================================================
!
! Macros for the complex pre-processing. All source files with extension
! .F90z are pre-processed twice. Once with 1 and then again without 1.
! Most instances of capital "Z" are replaced by either lower case "z" or
! lower case "d" respectively with or without 1.
! BE CAREFUL WITH CAPITAL "Z" IN .F90z FILES !!! IT HAS SPECIAL MEANING!!!
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
! Constants
! Implicit functions
! External functions and subroutines
! Internal functions and subroutines
! Internal arrays
!===================================================================
!===================================================================
!
! Printing routine. Prints kernel matrix elements in fort.* files.
! Useful only in debug mode.
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
subroutine zk_print(inode,m1,m2,m3,m4,k1,k2,k3,k4,rsp,csp,matel)

  use myconstants
  implicit none
  integer, intent(in) :: inode, m1, m2, m3, m4, k1, k2, k3, k4, rsp, csp
  complex(dpc), intent(in) :: matel

  integer, parameter :: ntape = 84
  integer :: nn

  nn = ntape + inode
  write(nn,'(4i6,6i3,2g20.10)') m1, m2, m3, m4, k1, k2, k3, k4, rsp, csp, &
       real(matel,dp), aimag(matel)

end subroutine zk_print
!===================================================================
