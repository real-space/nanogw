












!===================================================================
!
! Macros for the complex pre-processing. All source files with extension
! .F90z are pre-processed twice. Once with CPLX and then again without CPLX.
! Most instances of capital "Z" are replaced by either lower case "z" or
! lower case "d" respectively with or without CPLX.
! BE CAREFUL WITH CAPITAL "Z" IN .F90z FILES !!! IT HAS SPECIAL MEANING!!!
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
! Constants
! Implicit functions
! External functions and subroutines
! Internal functions and subroutines
! Internal arrays
!===================================================================
!===================================================================
!
! Module for FFT and related functions. All subroutines in this module
! are public and fairly independent from each other. The purpose of
! grouping them in a single module is that most of them use the same
! data: FFT boxes and Coulomb boxes.
!
! FFT is actually performed by external libraries. The coding below
! includes interface with four different librarires: FFTW 2.*, FFTW 3.*,
! MKL and ESSL. Other libraries can be used if you build the necessary
! interfaces (look at subroutines do_FFT, initialize_FFT, finalize_FFT,
! create_coul_0D, create_coul_1D, and adjust_fft).
!
! Notice that, contrary to FFTW and MKL, ESSL defines FFTs with an extra
! sign in the exponent (what do they want to be different from everybody
! else?). This is accounted for in the calls to dft3, drcft3, etc.
!
! Calls to initialize_FFT should precede calls to other subroutines in
! this module. Once FFT tasks are finished, one should make a call to
! finalize_FFT in order to deallocate data. If more FFTs are done at
! a later time (with the same grid size or not), initialize_FFT should
! be called again.
!
! Potentials are always evaluated in units of Ry, therefore e^2 = 2.
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
module fft_module

  use myconstants
  use typedefs

  public

  ! FFTW constants
  integer, parameter :: fftw_forward = -1
  integer, parameter :: fftw_backward = 1
  integer, parameter :: fftw_measure = 1

  ! Number of extra points between two consecutive grid points in real space
  ! (along directions y, z only). See subroutine create_coul_1D.
  integer, parameter :: n_in = 2
  ! Number of extra periodic cells beyond the original periodic cell
  ! (along directions y, z only).
  integer, parameter :: n_out = 2
  ! Zero-vector threshold. Vectors q with magnitude less than this are assumed
  ! to be zero.
  real(dp), parameter :: qzero = 1.d-6
  ! Euler-Mascheroni constant
  real(dp), parameter :: euler_m = 0.57721566490153286060651209_dp

  type fourier_transform
     ! shape of FFT grid, nfft(4) is used in real FFTs only
     integer :: nfft(4)
     ! length of FFT boxes
     integer :: n_size
     ! scale = 1/( nfft(1) * nfft(2) * nfft(3) )
     real(dp) :: scale
     ! cutoff radius (in units of Bohr radius) in Coulomb potential,
     ! potential is zero beyond this value
     real(dp) :: trunc
     ! Coulomb potential in reciprocal space (Ry units)
     real(dp), pointer :: dcoul(:,:,:)
     complex(dpc), pointer :: zcoul(:,:,:)
     ! FFT box
     real(dp), pointer :: dbox(:,:,:)
     complex(dpc), pointer :: zbox(:,:,:)

     ! FFTW plans, used by FFTW only
     integer (kind=8) :: plus_plan, minus_plan
  end type fourier_transform

  ! FFT data
  type (fourier_transform) :: fft_box

contains

  !===================================================================
  !
  ! For an input density rho, solves the Poisson equation in
  ! reciprocal space and returns the potential stored in variable rho.
  ! The Poisson equation is solved in reciprocal space, by doing FFT
  ! in the original density, multiplying that by the Coulomb potential,
  ! and doing FFT back-transform to get the final potential in real
  ! space.
  !
  ! INPUT:
  !    rho : density for which a potential will be computed
  !    irp : representation of input density (1 for fully symmetric densities)
  !
  ! OUTPUT:
  !    rho : computed potential
  !         V(r) = int_dr' rho(r') * e^2/|r - r'|
  !         Laplacian[ V(r) ] = -4 * pi * e^2 * rho(r)
  !
  !-------------------------------------------------------------------
  subroutine dpoisson(gvec,rho,irp)

    use typedefs
    implicit none

    ! arguments
    type (gspace), intent(in):: gvec
    real(dp), intent(inout) :: rho(gvec%nr)
    integer, intent(in) :: irp

    ! local variables
    integer :: jj, itr, gg(3)
    real(dp) :: tsec(2), charac(gvec%syms%ntrans)
    real(dp) :: xele, zscale

    !-------------------------------------------------------------------
    ! Put density in fft box.
    !
    fft_box%dbox = zero
    charac = real(gvec%syms%chi(irp,:),dp)
    do jj = 1, gvec%nr
       do itr = 1, gvec%syms%ntrans
          call unfold(gvec%r(1,jj),gvec%syms%trans(1,1,itr),gvec%shift,gg)
          fft_box%dbox(gg(1),gg(2),gg(3)) = rho(jj) * charac(itr)
       enddo
    end do
    !
    ! Fourier transform: box(r) -> FFT[ box(r) ] = box(G).
    !
    call timacc(11,1,tsec)
    call ddo_FFT(-1,fft_box)
    call timacc(11,2,tsec)
    !
    ! Solve Poisson's equation by multiplying it with the Coulomb potential.
    ! box(G) -> box(G) * 4 * pi * e^2 / G^2
    !
    call dmultiply_vec(fft_box%n_size,fft_box%dcoul,fft_box%dbox)
    !
    ! Inverse Fourier transform: box(G) = FFT[ box(r) ] -> box(r).
    !
    call timacc(11,1,tsec)
    call ddo_FFT(1,fft_box)
    call timacc(11,2,tsec)
    !
    ! Reconstruct rho in irreducible wedge and include the normalization
    ! factor.
    !
    rho = zero
    do jj = 1, gvec%nr
       gg = gvec%r(:,jj)
       rho(jj) = fft_box%dbox(gg(1),gg(2),gg(3))
    end do
    zscale = fft_box%scale
    call dscal(gvec%nr,zscale,rho,1)

  end subroutine dpoisson

  subroutine dfullpoisson(gvec,rho,outunit,verbose)
  use typedefs
  implicit none

    ! arguments
    type (gspace), intent(in):: gvec
    real(dp), intent(inout) :: rho(gvec%nr * gvec%syms%ntrans)
    integer, intent(in) :: outunit
    logical, intent(in) :: verbose

    ! local variables
    integer :: jj, itr, gg(3), ii, kk, count
    real(dp) :: tsec(2)
    real(dp) :: xele, zscale

    !-------------------------------------------------------------------
    ! Put density in fft box.
    !
    ! Here rho is stored in full grid
    fft_box%dbox = zero
    do jj = 1, gvec%nr
       do itr = 1, gvec%syms%ntrans
          call unfold(gvec%r(1,jj),gvec%syms%trans(1,1,itr),gvec%shift,gg)
          fft_box%dbox(gg(1),gg(2),gg(3)) = rho((jj-1)*gvec%syms%ntrans + itr) 
       enddo
    end do
    count=0
    if(verbose) then
       write(outunit,*) " inside zfullpoisson "
       write(outunit,*) " fft%Zcoul = "
       do ii = -fft_box%nfft(1)/2,fft_box%nfft(1)/2+1
          do jj = -fft_box%nfft(2)/2,fft_box%nfft(2)/2-1
             do kk = -fft_box%nfft(3)/2,fft_box%nfft(3)/2-1
                count = count + 1
                if(abs(fft_box%dbox(ii,jj,kk))>0.01) &
                    write(outunit, '(3i7,f20.8)') ii,jj,kk,fft_box%dcoul(ii,jj,kk)
             enddo
          enddo
       enddo
    endif
    !
    ! Fourier transform: box(r) -> FFT[ box(r) ] = box(G).
    !
    call timacc(11,1,tsec)
    call ddo_FFT(-1,fft_box)
    call timacc(11,2,tsec)
    !
    ! Solve Poisson's equation by multiplying it with the Coulomb potential.
    ! box(G) -> box(G) * 4 * pi * e^2 / G^2
    !
    call dmultiply_vec(fft_box%n_size,fft_box%dcoul,fft_box%dbox)
    !
    ! Inverse Fourier transform: box(G) = FFT[ box(r) ] -> box(r).
    !
    call timacc(11,1,tsec)
    call ddo_FFT(1,fft_box)
    call timacc(11,2,tsec)
    if(verbose) then
       write(outunit,*) "After fft, fft%Zbox = "
       count=0
       do ii = -fft_box%nfft(1)/2,fft_box%nfft(1)/2+1
          do jj = -fft_box%nfft(2)/2,fft_box%nfft(2)/2-1
             do kk = -fft_box%nfft(3)/2,fft_box%nfft(3)/2-1
                count = count + 1
                if(abs(fft_box%dbox(ii,jj,kk)*fft_box%scale)>1.00) &
                    write(outunit, '(3i7,f20.8)') ii,jj,kk, &
                      fft_box%dbox(ii,jj,kk)*fft_box%scale
             enddo
          enddo
       enddo
    endif
    !
    ! Reconstruct rho in irreducible wedge and include the normalization
    ! factor.
    !
    rho = zero
    zscale = fft_box%scale
    if(verbose) write(outunit,*) "zscale: ", zscale
    if(verbose) write(outunit,*) "rho = "
    do jj = 1, gvec%nr
       do itr = 1,gvec%syms%ntrans
          call unfold(gvec%r(1,jj),gvec%syms%trans(1,1,itr),gvec%shift,gg)
          rho((jj-1)*gvec%syms%ntrans + itr) = fft_box%dbox(gg(1),gg(2),gg(3))
       enddo
    end do
    call dscal(gvec%nr*gvec%syms%ntrans,zscale,rho,1)
    do jj = 1, gvec%nr
       do itr = 1,gvec%syms%ntrans
          call unfold(gvec%r(1,jj),gvec%syms%trans(1,1,itr),gvec%shift,gg)
          if(verbose .and. abs(rho((jj-1)*gvec%syms%ntrans + itr))>1.0) &
              write(outunit,'(6i4,i8,f14.7)') & 
              gvec%r(1,jj),gvec%r(2,jj),gvec%r(3,jj),gg(1),gg(2),gg(3),(jj-1)*gvec%syms%ntrans+itr,rho((jj-1)*gvec%syms%ntrans + itr)
       enddo
    end do

  end subroutine dfullpoisson

  !===================================================================
  !
  ! Do an FFT in place: destroys contents of dbox
  ! and replaces them by the Fourier transform.
  !
  ! The FFT done is:
  !
  !   dbox(p) <- sum_j { dbox(j)*e^{sign*i*j.p} }
  !
  ! where j and p are integer 3 vectors ranging over Nfft(1:3).
  !
  ! MKL has a problem with argument overloading, so it does out-of-place
  ! FFT instead.
  !
  ! FFTW with real algebra has a funny data layout which requires a 4-th
  ! dimension ( nfft(4) ).
  !
  !-------------------------------------------------------------------
  subroutine ddo_FFT(sign,fft)

    use myconstants
    implicit none
    ! direction: sign = 1 does FFT from reciprocal space to real space
    !           sign = -1 does FFT from real space to reciprocal space
    integer, intent(in) :: sign
    ! FFT data
    type (fourier_transform), intent(inout) :: fft

    ! local variables

    if (sign /= 1 .and. sign /= -1) &
         call die('sign is not 1 or -1 in do_FFT')

    if (sign == 1) then
       call dfftw_execute( fft%plus_plan )
    else if (sign == -1) then
       call dfftw_execute( fft%minus_plan )
    endif

  end subroutine ddo_FFT
  !===================================================================
  !
  ! Initialize FFT environment. All arrays in structure "fft" are allocated
  ! here. Value of fft%trunc and fft%nfft are defined in parent routine.
  !
  !-------------------------------------------------------------------
  subroutine dinitialize_FFT(inode,fft)

    use myconstants
    implicit none

    ! arguments
    ! rank of current PE, for I/O only
    integer, intent(in) :: inode
    ! FFT data
    type (fourier_transform), intent(inout) :: fft

    ! local variables
    logical :: verbose
    integer :: info
    character (len=100) :: str

    verbose = .false.
    if (inode == 0) verbose = .true.
    fft%scale = one/((one*fft%nfft(1))*(one*fft%nfft(2))*(one*fft%nfft(3)))

    ! Allocate boxes: dbox holds densities, dcoul holds the Coulomb potential.
    fft%n_size = fft%nfft(4) * fft%nfft(2) * fft%nfft(3)
    allocate( fft%dbox(-fft%nfft(1)/2:fft%nfft(1)/2+1, &
                     -fft%nfft(2)/2:fft%nfft(2)/2-1, &
                     -fft%nfft(3)/2:fft%nfft(3)/2-1), stat=info)
    call alccheck('Zbox','intialize_FFT',fft%n_size,info)

    allocate( fft%dcoul(-fft%nfft(1)/2:fft%nfft(1)/2+1, &
                    -fft%nfft(2)/2:fft%nfft(2)/2-1, &
                    -fft%nfft(3)/2:fft%nfft(3)/2-1), stat=info)
    call alccheck('fft%Zcoul','initialize_FFT',fft%n_size,info)

    ! Initialize FFTW3 plans.
    write(str,'(a,3(i4,a))') ' Creating ', &
         fft%nfft(1),' x ',fft%nfft(2),' x ',fft%nfft(3),' FFTW 3 plans.'
    call stopwatch(verbose,str)
    call dfftw_plan_dft_c2r_3d(fft%plus_plan,fft%nfft(1),fft%nfft(2), &
         fft%nfft(3),fft%dbox,fft%dbox, fftw_measure)
    call dfftw_plan_dft_r2c_3d(fft%minus_plan,fft%nfft(1),fft%nfft(2), &
         fft%nfft(3),fft%dbox,fft%dbox, fftw_measure)

  end subroutine dinitialize_FFT
  !===================================================================
  !
  ! Undefine all FFT variables, including V_coul.
  !
  !-------------------------------------------------------------------
  subroutine dfinalize_FFT(inode,fft)

    use myconstants
    implicit none

    ! arguments
    ! rank of current PE, for I/O only
    integer, intent(in) :: inode
    ! FFT data
    type (fourier_transform), intent(inout) :: fft

    ! local variables
    logical :: verbose

    verbose = .false.
    if (inode == 0) verbose = .true.
    if (associated(fft%dcoul)) deallocate(fft%dcoul)
    if (associated(fft%dbox)) deallocate(fft%dbox)
    fft%scale = zero
    fft%n_size = 0
    call dfftw_destroy_plan(fft%minus_plan)
    call dfftw_destroy_plan(fft%plus_plan)
    call stopwatch(verbose,'FFT finalized')

  end subroutine dfinalize_FFT
  !===================================================================
  !
  ! Calculate Coulomb interaction,
  !        V_coul(G) = 4*pi*e^2/(q+G)^2 (rydberg units, e^2 = 2). 
  ! Use the same layout as the FFT boxes in subroutine "poisson".
  ! That saves reordering of arrays in the product dbox * dcoul.
  ! If the Coulomb potential is not truncated in real space
  ! (i.e., fft%trunc <= 0), then its singularity in reciprocal space
  ! must be removed. We do that by setting V_coul = 0 when |q+G| < qzero.
  ! Input vector q is given in units of reciprocal lattice vectors. This
  ! subroutine performs spherical truncation, so it is suitable for confined
  ! systems (with truncation) or bulk systems (without truncation). The
  ! truncation algorithm is presented in several articles, see for example:
  ! E. L. de la Grandmaison et al., Comput. Phys. Commun. 167, 7 (2005).
  !
  !-------------------------------------------------------------------
  subroutine dcreate_coul_0D(bdot,qq,fft)

    use myconstants
    implicit none

    ! arguments
    ! reciprocal space metric, q-vector in units of reciprocal lattice vectors
    real(dp), intent(in) :: bdot(3,3), qq(3)
    ! FFT data
    type (fourier_transform), intent(inout) :: fft

    ! local variables
    integer :: i1, i2, i3, j1, j2, j3
    real(dp) :: ekin, qpg(3)
    real(dp) :: rtmp

    fft%dcoul = zero

    do j3 = -fft%nfft(3)/2, fft%nfft(3)/2 - 1
       qpg(3) = j3 + qq(3)
       i3 = j3 - fft%nfft(3)/2
       if (j3 < 0) i3 = fft%nfft(3) + i3
       do j2 = -fft%nfft(2)/2, fft%nfft(2)/2 - 1
          qpg(2) = j2 + qq(2)
          i2 = j2 - fft%nfft(2)/2
          if (j2 < 0) i2 = fft%nfft(2) + i2
          do j1 = 0, fft%nfft(1)/2
             qpg(1) = j1 + qq(1)
             i1 = j1 - fft%nfft(1)/2
             if (j1 < 0) i1 = fft%nfft(1) + i1

             ekin = dot_product(qpg,matmul(bdot,qpg))

             if (fft%trunc > zero) then
                if (ekin < qzero*qzero) then
                   rtmp = fft%trunc**2/two
                else
                   rtmp = (one-cos(fft%trunc*sqrt(ekin))) / ekin
                endif
             else
                if (ekin < qzero*qzero) then
                   rtmp = zero
                else
                   rtmp = one / ekin
                endif
             endif
             fft%dcoul(2*i1+fft%nfft(1)/2,i2,i3) = rtmp
             fft%dcoul(2*i1+fft%nfft(1)/2+1,i2,i3) = rtmp
          enddo
       enddo
    enddo

    rtmp = eight * pi
    call dscal(fft%n_size,rtmp,fft%dcoul,1)

  end subroutine dcreate_coul_0D
  !===================================================================
  !
  ! Calculate the Coulomb interaction with wire boundary conditions. The
  ! system is assumed to be periodic along x direction and confined along
  ! y and z directions. V_coul is calculated using truncation of periodic
  ! images following this method:
  ! S. Ismail-Beigi, Phys. Rev. B 73, 233103 (2006).
  !
  ! The Coulomb potential is calculated through 2-D FFT on the extended cell.
  !
  ! n_in is a (positive integer) parameter for real-space resolution.
  ! If the energy cutoff is too small, then the FFT grid may not be very
  ! dense and the Coulomb singularity at r = 0 may be poorly handled.
  ! Larger values of n_in will add extra points in between the original
  ! FFT grid. If the original grid has N points covering a space of length
  ! L, then the grid used to do 2-dimensional FFTs will have N * n_in points
  ! covering a space of length L.
  !
  ! When V_coul(G_1 + q,y,z) is calculated, we add a shift on the grid in
  ! order to avoid the singularity at y = z = 0.
  !
  ! Output is truncated dcoul(G_1+q,G_2,G_3) in reciprocal space such that,
  ! without truncation, its value would be:
  !
  !             dcoul(ig) = 8 Pi/(q + G)*2
  !
  ! for each G-vector in the FFT box.
  !
  ! Calculation is distributed over processors but array dcoul returns
  ! with global value.
  !
  !-------------------------------------------------------------------
  subroutine dcreate_coul_1D(inode,npes,comm,qq,gvec,fft)

    use typedefs
    use mpi_module
    implicit none

    ! arguments
    ! 1 data: PE rank, number of PEs, communicator
    integer, intent(in) :: inode, npes, comm
    ! q-vector, in units of reciprocal lattice vectors
    real(dp), intent(in) :: qq
    ! real-space grid
    type (gspace), intent(in):: gvec
    ! FFT data
    type (fourier_transform), intent(inout) :: fft

    ! local variables
    character (len=100) :: str
    logical :: verbose
    integer :: info, i1, i2, i3, j1, j2, j3, l2, l3, nfft_4, dNfft(3)
    real(dp) :: g1, gr(3), bdot(3,3), step(3)
!    real(dp) :: xdummy, bk0, bk1
    real(dp) :: zscale
    real(dp), pointer :: box_1D(:,:)

    ! FFTW plans, used by FFTW only
    integer (kind=8) :: minus_plan_1D

    verbose = .false.
    if (inode == 0) verbose = .true.

    ! Initialize two-dimensional FFT grid.
    dNfft(1) = fft%nfft(1)
    dNfft(2) = fft%nfft(2) * n_in * n_out
    dNfft(3) = fft%nfft(3) * n_in * n_out
    nfft_4 = dNfft(2) + 2

    ! Initialize real-space metric in the yz plane. Assume that directions
    ! 2 and 3 are orthogonal (i.e. bdot(2,3) = bdot(3,2) = 0)
    step(1) = gvec%step(1)
    step(2) = gvec%step(2) / real( n_in, dp)
    step(3) = gvec%step(3) / real( n_in, dp)

    bdot = gvec%bdot
    bdot(2,2) = (two * pi / (step(2)*dNfft(2)))**2
    bdot(3,3) = (two * pi / (step(3)*dNfft(3)))**2

    ! Initialize boxes.
    allocate( box_1D(-dNfft(2)/2:dNfft(2)/2+1, &
                        -dNfft(3)/2:dNfft(3)/2-1), stat=info)
    i1 = dNfft(2) * dNfft(3)
    call alccheck('box_1D','create_coul_1D',i1,info)

    fft%dcoul = zero
    if (verbose) then
       write(6,'(/,a)') ' Building truncated Coulomb potential using 2-D FFTs.'
       write(6,*) ' FFT grid = ', dNfft(2:3)
       write(6,*) ' Extra points between two points in 3-D FFT grid = ', n_in
       write(6,*) ' Number of y-z supercells = ', n_out
       write(6,*)
    endif

    ! Initialize FFTW3 plans.
    write(str,'(a,3(i4,a))') 'Creating ', &
         dNfft(2),' x',dNfft(3),' FFTW 3 plans '
    call stopwatch(verbose,str)
    call dfftw_plan_dft_r2c_2d(minus_plan_1D,dNfft(2), &
         dNfft(3),box_1D,box_1D, fftw_measure)
    !
    ! Start loop over planes of yz points.
    !
    do j1 = 0 + inode, fft%nfft(1)/2, npes
       g1 = abs(j1 + qq) * two * pi / gvec%avec(1,1)
       i1 = j1 - fft%nfft(1)/2
       if (j1 < 0) i1 = fft%nfft(1) + i1

       box_1D = zero
       !
       ! For each plane, calculate the potential V_trunc(G_1+q,y,z).
       ! The potential is not zero only inside the Wigner-Seitz cell.
       ! Because of the logarithmic singularity, we must separate the case
       ! G_1 + q = 0.
       !
       if (g1 < qzero) then
          do j3 = -dNfft(3)/2/n_out, dNfft(3)/2/n_out - 1
             gr(3) = (j3 + gvec%shift(3)) * step(3)
             i3 = j3 - dNfft(3)/2
             if (j3 < 0) i3 = dNfft(3) + i3
             do j2 = -dNfft(2)/2/n_out, dNfft(2)/2/n_out - 1
                gr(2) = (j2 + gvec%shift(2)) * step(2)
                i2 = j2 - dNfft(2)/2
                if (j2 < 0) i2 = dNfft(2) + i2
                box_1D(i2,i3) = -log( sqrt(dot_product(gr(2:3),gr(2:3))) )
             enddo
          enddo
       else
          do j3 = -dNfft(3)/2/n_out, dNfft(3)/2/n_out - 1
             gr(3) = (j3 + gvec%shift(3)) * step(3)
             i3 = j3 - dNfft(3)/2
             if (j3 < 0) i3 = dNfft(3) + i3
             do j2 = -dNfft(2)/2/n_out, dNfft(2)/2/n_out - 1
                gr(2) = (j2 + gvec%shift(2)) * step(2)
                i2 = j2 - dNfft(2)/2
                if (j2 < 0) i2 = dNfft(2) + i2
!                xdummy = sqrt(dot_product(gr(2:3),gr(2:3))) * g1
!                call bessel_k(xdummy,bk0,bk1)
!                box_1D(i2,i3) = bk0
                box_1D(i2,i3) = &
                     bessel_k0( sqrt(dot_product(gr(2:3),gr(2:3))) * g1 )
             enddo
          enddo
       endif
       !
       ! Do a two-dimensional Fourier transform:
       !  V_trunc(G_1+q,y,z) -> V_trunc(G_1+q,G_2,G_3)
       !
       call dfftw_execute( minus_plan_1D )
       !
       ! Store the truncated potential V_trunc in dcoul.
       !
       do j3 = -fft%nfft(3)/2, fft%nfft(3)/2 - 1
          l3 = j3 - fft%nfft(3)/2
          if (j3 < 0) l3 = fft%nfft(3) + l3
          i3 = j3 * n_out - dNfft(3)/2 
          if (j3 < 0) i3 = dNfft(3) + i3
          do j2 = -fft%nfft(2)/2, - 1
             l2 = j2 + fft%nfft(2)/2
             i2 = j2 * n_out + dNfft(2)/2
             fft%dcoul(2*j1-fft%nfft(1)/2,l2,l3) = box_1D(-2*i2+dNfft(2)/2,i3)
             fft%dcoul(2*j1-fft%nfft(1)/2+1,l2,l3) = box_1D(-2*i2+dNfft(2)/2,i3)
          enddo
          do j2 = 0, fft%nfft(2)/2 - 1
             l2 = j2 - fft%nfft(2)/2
             i2 = j2 * n_out - dNfft(2)/2
             fft%dcoul(2*j1-fft%nfft(1)/2,l2,l3) = box_1D(2*i2+dNfft(2)/2,i3)
             fft%dcoul(2*j1-fft%nfft(1)/2+1,l2,l3) = box_1D(2*i2+dNfft(2)/2,i3)
          enddo
       enddo
    enddo
    !
    ! Remove the long-wavelength (G1 = G2 = G3 = 0) part.
    !
    if (abs(qq) < qzero) then
       fft%dcoul(-fft%nfft(1)/2,-fft%nfft(2)/2,-fft%nfft(3)/2) = zero
       fft%dcoul(-fft%nfft(1)/2+1,-fft%nfft(2)/2,-fft%nfft(3)/2) = zero
    endif
    !
    ! Global reduction if there is more than one processor. And rescale dcoul
    ! with the area of the unit cell in the yz plane.
    !
    call dpsum(fft%n_size,npes,comm,fft%dcoul)
    zscale = one * four * step(2) * step(3)
    call dscal(fft%n_size,zscale,fft%dcoul,1)

    call dfftw_destroy_plan( minus_plan_1D )

    deallocate(box_1D)

  end subroutine dcreate_coul_1D
  !===================================================================
  !
  ! Calculate the Coulomb interaction with slab boundary conditions. The
  ! system is assumed to be periodic along x and y directions and confined
  ! along z direction. V_coul is calculated using truncation of periodic
  ! images following this method:
  ! S. Ismail-Beigi, Phys. Rev. B 73, 233103 (2006).
  !
  ! The Coulomb potential is calculated using Eq. 3 in the article above.
  !
  !-------------------------------------------------------------------
  subroutine dcreate_coul_2D(bdot,qq,fft)

    use typedefs
    implicit none

    ! arguments
    ! reciprocal space metric, q-vector in units of reciprocal lattice vectors
    real(dp), intent(in) :: bdot(3,3), qq(2)
    ! FFT data
    type (fourier_transform), intent(inout) :: fft

    ! local variables
    integer :: i1, i2, i3, j1, j2, j3, cosgz
    real(dp) :: gz, qpg2, qpg(2), bdot_2d(2,2)
    real(dp) :: rtmp

    fft%dcoul = zero
    bdot_2d = bdot(1:2,1:2)

    do j3 = -fft%nfft(3)/2, fft%nfft(3)/2 - 1
       gz = j3 * pi / fft%trunc
       cosgz = 1 - 2 * mod(j3,2)
       i3 = j3 - fft%nfft(3)/2
       if (j3 < 0) i3 = fft%nfft(3) + i3
       do j2 = -fft%nfft(2)/2, fft%nfft(2)/2 - 1
          qpg(2) = j2 + qq(2)
          i2 = j2 - fft%nfft(2)/2
          if (j2 < 0) i2 = fft%nfft(2) + i2
          do j1 = 0, fft%nfft(1)/2
             qpg(1) = j1 + qq(1)
             i1 = j1 - fft%nfft(1)/2
             if (j1 < 0) i1 = fft%nfft(1) + i1

             qpg2 = dot_product(qpg,matmul(bdot_2d,qpg))
             if (qpg2 < qzero*qzero .and. j3 == 0) cycle
             rtmp = (one - exp(-sqrt(qpg2)*fft%trunc)*cosgz) / (qpg2 + gz*gz)

             fft%dcoul(2*i1+fft%nfft(1)/2,i2,i3) = rtmp
             fft%dcoul(2*i1+fft%nfft(1)/2+1,i2,i3) = rtmp
          enddo
       enddo
    enddo

    rtmp = eight * pi
    call dscal(fft%n_size,rtmp,fft%dcoul,1)

  end subroutine dcreate_coul_2D
  !===================================================================
  !
  ! Calculate the phase offset, bphase = exp( i qq.r ),
  ! where qq = k4 - k3 + k1 - k2.
  ! This is needed only in periodic systems. For confined systems
  ! (CPLX disabled) it returns bphase = one. Vector qq is assumed to
  ! be equal to some reciprocal lattice point.
  !
  !-------------------------------------------------------------------
  subroutine dget_phase(gvec,istart,nelem,qq,bphase)

    use myconstants
    implicit none

    ! arguments
    ! real-space grid
    type (gspace), intent(inout):: gvec
    ! address of first grid point in bphase, number of grid points in bphase
    integer, intent(in) :: istart, nelem
    ! q-vector, in units of reciprocal lattice vectors
    real(dp), intent(in) :: qq(3)
    ! phase, calculated only at grid points (istart+1) through (istart+nelem)
    real(dp), intent(out) :: bphase(nelem)


    bphase = one


  end subroutine dget_phase
  !===================================================================
  !
  ! Calculate the negative of the Laplacian using FFT.
  !
  ! INPUT:
  !   given_func : function f(r) on the real-space grid
  !   irp : irreducible representation of f(r), 1 for fully symmetric function.
  !
  ! OUTPUT:
  !   laplf : -Laplacian[ f(r) ] on the real-space grid
  !
  !-------------------------------------------------------------------
  subroutine dget_lap_FFT(gvec,given_func,laplf,irp)

    use typedefs
    implicit none

    ! arguments
    type (gspace), intent(inout):: gvec
    real(dp), intent(in) :: given_func(gvec%nr)
    real(dp), intent(out) :: laplf(gvec%nr)
    integer, intent(in) :: irp

    ! local variables
    integer :: i1, i2, i3, j1, j2, j3, jj, itr, gg(3)
    real(dp) :: tsec(2), charac(gvec%syms%ntrans), ekin
    real(dp) :: zscale

    !-------------------------------------------------------------------
    ! Put input function in fft box.
    !
    fft_box%dbox = zero
    charac = real(gvec%syms%chi(irp,:),dp)
    do jj = 1, gvec%nr
       do itr = 1, gvec%syms%ntrans
          call unfold(gvec%r(1,jj),gvec%syms%trans(1,1,itr),gvec%shift,gg)
          fft_box%dbox(gg(1),gg(2),gg(3)) = given_func(jj) * charac(itr)
       enddo
    end do
    !
    ! Fourier transform: box(r) -> FFT[ box(r) ] = box(G).
    !
    call timacc(11,1,tsec)
    call ddo_FFT(-1,fft_box)
    call timacc(11,2,tsec)
    !
    ! Calculate Laplacian in reciprocal space: box(G) -> box(G) * G^2.
    !
    do j3 = -fft_box%nfft(3)/2, fft_box%nfft(3)/2 - 1
       gg(3) = j3
       i3 = j3 - fft_box%nfft(3)/2
       if (j3 < 0) i3 = fft_box%nfft(3) + i3
       do j2 = -fft_box%nfft(2)/2, fft_box%nfft(2)/2 - 1
          gg(2) = j2
          i2 = j2 - fft_box%nfft(2)/2
          if (j2 < 0) i2 = fft_box%nfft(2) + i2
          do j1 = -fft_box%nfft(1)/2, fft_box%nfft(1)/2 - 1
             gg(1) = j1
             i1 = j1 - fft_box%nfft(1)/2
             if (j1 < 0) i1 = fft_box%nfft(1) + i1
             ekin = dot_product(gg,matmul(gvec%bdot,gg))
             fft_box%dbox(i1,i2,i3) = fft_box%dbox(i1,i2,i3) * ekin
          enddo
       enddo
    enddo
    !
    ! Inverse Fourier transform: box(G) = FFT[ box(r) ] -> box(r).
    !
    call timacc(11,1,tsec)
    call ddo_FFT(1,fft_box)
    call timacc(11,2,tsec)
    !
    ! Store result in output array, laplf(r), and change its sign.
    !
    do jj = 1, gvec%nr
       gg = gvec%r(:,jj)
       laplf(jj) = fft_box%dbox(gg(1),gg(2),gg(3))
    end do
    zscale = -fft_box%scale
    call dscal(gvec%nr,zscale,laplf,1)

  end subroutine dget_lap_FFT
  !===================================================================
  !
  ! Calculate the gradient using FFT. Since a generic function and its
  ! gradient do not always belong to the same irreducible representation,
  ! the output gradient is calculated on the full grid, not only the
  ! irreducible wedge.
  !
  ! INPUT:
  !   given_func : function f(r) on the real-space grid
  !   irp : irreducible representation of f(r), 1 for fully symmetric function.
  !
  ! OUTPUT:
  !   gradf : Gradient[ f(r) ] on the real-space grid
  !
  !-------------------------------------------------------------------
  subroutine dget_grad_FFT(gvec,given_func,gradf,irp)

    use typedefs
    implicit none

    ! arguments
    type (gspace), intent(inout):: gvec
    real(dp), intent(in) :: given_func(gvec%nr)
    real(dp), intent(out) :: gradf(3,gvec%nr*gvec%syms%ntrans)
    integer, intent(in) :: irp

    ! local variables
    integer :: i1, i2, i3, j1, j2, j3, jj, itr, idir, gg(3)
    real(dp) :: tsec(2), charac(gvec%syms%ntrans), rr(3), rrp(3)
    real(dp) :: zscale

    do idir = 1, 3
       !-------------------------------------------------------------------
       ! Put input function in fft box.
       !
       fft_box%dbox = zero
       charac = real(gvec%syms%chi(irp,:),dp)
       do jj = 1, gvec%nr
          do itr = 1, gvec%syms%ntrans
             call unfold(gvec%r(1,jj),gvec%syms%trans(1,1,itr),gvec%shift,gg)
             fft_box%dbox(gg(1),gg(2),gg(3)) = given_func(jj) * charac(itr)
          enddo
       end do
       !
       ! Fourier transform: box(r) -> FFT[ box(r) ] = box(G).
       !
       call timacc(11,1,tsec)
       call ddo_FFT(-1,fft_box)
       call timacc(11,2,tsec)
       !
       ! Calculate derivative in reciprocal space: box(G) -> box(G) * G.
       !
       do j3 = -fft_box%nfft(3)/2, fft_box%nfft(3)/2 - 1
          rr(3) = j3
          i3 = j3 - fft_box%nfft(3)/2
          if (j3 < 0) i3 = fft_box%nfft(3) + i3
          do j2 = -fft_box%nfft(2)/2, fft_box%nfft(2)/2 - 1
             rr(2) = j2
             i2 = j2 - fft_box%nfft(2)/2
             if (j2 < 0) i2 = fft_box%nfft(2) + i2
             do j1 = -fft_box%nfft(1)/2, fft_box%nfft(1)/2 - 1
                rr(1) = j1
                i1 = j1 - fft_box%nfft(1)/2
                if (j1 < 0) i1 = fft_box%nfft(1) + i1
                call dmatvec3('N',gvec%bvec,rr,rrp)
                fft_box%dbox(i1,i2,i3) = &
                     fft_box%dbox(i1,i2,i3) * rrp(idir)
             enddo
          enddo
       enddo
       !
       ! Inverse Fourier transform: box(G) = FFT[ box(r) ] -> box(r).
       !
       call timacc(11,1,tsec)
       call ddo_FFT(1,fft_box)
       call timacc(11,2,tsec)
       !
       ! Store result in output array, gradf(r).
       !
       charac = real(gvec%syms%chi(irp,:),dp)
       do jj = 1, gvec%nr
          do itr = 1, gvec%syms%ntrans
             call unfold(gvec%r(1,jj),gvec%syms%trans(1,1,itr),gvec%shift,gg)
             gradf(idir,jj + (itr-1)*gvec%nr) = &
                  fft_box%dbox(gg(1),gg(2),gg(3)) * charac(itr)
          enddo
       end do
    enddo

    zscale = fft_box%scale
    call dscal(3*gvec%nr*gvec%syms%ntrans,zscale,gradf,1)

  end subroutine dget_grad_FFT
  !===================================================================













!===================================================================
!
! Macros for the complex pre-processing. All source files with extension
! .F90z are pre-processed twice. Once with 1 and then again without 1.
! Most instances of capital "Z" are replaced by either lower case "z" or
! lower case "d" respectively with or without 1.
! BE CAREFUL WITH CAPITAL "Z" IN .F90z FILES !!! IT HAS SPECIAL MEANING!!!
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
! Constants
! Implicit functions
! External functions and subroutines
! Internal functions and subroutines
! Internal arrays
!===================================================================
!===================================================================
!
! Module for FFT and related functions. All subroutines in this module
! are public and fairly independent from each other. The purpose of
! grouping them in a single module is that most of them use the same
! data: FFT boxes and Coulomb boxes.
!
! FFT is actually performed by external libraries. The coding below
! includes interface with four different librarires: FFTW 2.*, FFTW 3.*,
! MKL and ESSL. Other libraries can be used if you build the necessary
! interfaces (look at subroutines do_FFT, initialize_FFT, finalize_FFT,
! create_coul_0D, create_coul_1D, and adjust_fft).
!
! Notice that, contrary to FFTW and MKL, ESSL defines FFTs with an extra
! sign in the exponent (what do they want to be different from everybody
! else?). This is accounted for in the calls to dft3, drcft3, etc.
!
! Calls to initialize_FFT should precede calls to other subroutines in
! this module. Once FFT tasks are finished, one should make a call to
! finalize_FFT in order to deallocate data. If more FFTs are done at
! a later time (with the same grid size or not), initialize_FFT should
! be called again.
!
! Potentials are always evaluated in units of Ry, therefore e^2 = 2.
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
  !===================================================================
  !
  ! For an input density rho, solves the Poisson equation in
  ! reciprocal space and returns the potential stored in variable rho.
  ! The Poisson equation is solved in reciprocal space, by doing FFT
  ! in the original density, multiplying that by the Coulomb potential,
  ! and doing FFT back-transform to get the final potential in real
  ! space.
  !
  ! INPUT:
  !    rho : density for which a potential will be computed
  !    irp : representation of input density (1 for fully symmetric densities)
  !
  ! OUTPUT:
  !    rho : computed potential
  !         V(r) = int_dr' rho(r') * e^2/|r - r'|
  !         Laplacian[ V(r) ] = -4 * pi * e^2 * rho(r)
  !
  !-------------------------------------------------------------------
  subroutine zpoisson(gvec,rho,irp)

    use typedefs
    implicit none

    ! arguments
    type (gspace), intent(in):: gvec
    complex(dpc), intent(inout) :: rho(gvec%nr)
    integer, intent(in) :: irp

    ! local variables
    integer :: jj, itr, gg(3)
    real(dp) :: tsec(2), charac(gvec%syms%ntrans)
    complex(dpc) :: xele, zscale

    !-------------------------------------------------------------------
    ! Put density in fft box.
    !
    fft_box%zbox = zzero
    charac = real(gvec%syms%chi(irp,:),dp)
    do jj = 1, gvec%nr
       do itr = 1, gvec%syms%ntrans
          call unfold(gvec%r(1,jj),gvec%syms%trans(1,1,itr),gvec%shift,gg)
          fft_box%zbox(gg(1),gg(2),gg(3)) = rho(jj) * charac(itr)
       enddo
    end do
    !
    ! Fourier transform: box(r) -> FFT[ box(r) ] = box(G).
    !
    call timacc(11,1,tsec)
    call zdo_FFT(-1,fft_box)
    call timacc(11,2,tsec)
    !
    ! Solve Poisson's equation by multiplying it with the Coulomb potential.
    ! box(G) -> box(G) * 4 * pi * e^2 / G^2
    !
    call zmultiply_vec(fft_box%n_size,fft_box%zcoul,fft_box%zbox)
    !
    ! Inverse Fourier transform: box(G) = FFT[ box(r) ] -> box(r).
    !
    call timacc(11,1,tsec)
    call zdo_FFT(1,fft_box)
    call timacc(11,2,tsec)
    !
    ! Reconstruct rho in irreducible wedge and include the normalization
    ! factor.
    !
    rho = zzero
    do jj = 1, gvec%nr
       gg = gvec%r(:,jj)
       rho(jj) = fft_box%zbox(gg(1),gg(2),gg(3))
    end do
    zscale = fft_box%scale
    call zscal(gvec%nr,zscale,rho,1)

  end subroutine zpoisson

  subroutine zfullpoisson(gvec,rho,outunit,verbose)
  use typedefs
  implicit none

    ! arguments
    type (gspace), intent(in):: gvec
    complex(dpc), intent(inout) :: rho(gvec%nr * gvec%syms%ntrans)
    integer, intent(in) :: outunit
    logical, intent(in) :: verbose

    ! local variables
    integer :: jj, itr, gg(3), ii, kk, count
    real(dp) :: tsec(2)
    complex(dpc) :: xele, zscale

    !-------------------------------------------------------------------
    ! Put density in fft box.
    !
    ! Here rho is stored in full grid
    fft_box%zbox = zzero
    do jj = 1, gvec%nr
       do itr = 1, gvec%syms%ntrans
          call unfold(gvec%r(1,jj),gvec%syms%trans(1,1,itr),gvec%shift,gg)
          fft_box%zbox(gg(1),gg(2),gg(3)) = rho((jj-1)*gvec%syms%ntrans + itr) 
       enddo
    end do
    count=0
    if(verbose) then
       write(outunit,*) " inside zfullpoisson "
       write(outunit,*) " fft%Zcoul = "
       do ii = -fft_box%nfft(1)/2,fft_box%nfft(1)/2+1
          do jj = -fft_box%nfft(2)/2,fft_box%nfft(2)/2-1
             do kk = -fft_box%nfft(3)/2,fft_box%nfft(3)/2-1
                count = count + 1
                if(abs(fft_box%zbox(ii,jj,kk))>0.01) &
                    write(outunit, '(3i7,f20.8)') ii,jj,kk,fft_box%zcoul(ii,jj,kk)
             enddo
          enddo
       enddo
    endif
    !
    ! Fourier transform: box(r) -> FFT[ box(r) ] = box(G).
    !
    call timacc(11,1,tsec)
    call zdo_FFT(-1,fft_box)
    call timacc(11,2,tsec)
    !
    ! Solve Poisson's equation by multiplying it with the Coulomb potential.
    ! box(G) -> box(G) * 4 * pi * e^2 / G^2
    !
    call zmultiply_vec(fft_box%n_size,fft_box%zcoul,fft_box%zbox)
    !
    ! Inverse Fourier transform: box(G) = FFT[ box(r) ] -> box(r).
    !
    call timacc(11,1,tsec)
    call zdo_FFT(1,fft_box)
    call timacc(11,2,tsec)
    if(verbose) then
       write(outunit,*) "After fft, fft%Zbox = "
       count=0
       do ii = -fft_box%nfft(1)/2,fft_box%nfft(1)/2+1
          do jj = -fft_box%nfft(2)/2,fft_box%nfft(2)/2-1
             do kk = -fft_box%nfft(3)/2,fft_box%nfft(3)/2-1
                count = count + 1
                if(abs(fft_box%zbox(ii,jj,kk)*fft_box%scale)>1.00) &
                    write(outunit, '(3i7,f20.8)') ii,jj,kk, &
                      fft_box%zbox(ii,jj,kk)*fft_box%scale
             enddo
          enddo
       enddo
    endif
    !
    ! Reconstruct rho in irreducible wedge and include the normalization
    ! factor.
    !
    rho = zzero
    zscale = fft_box%scale
    if(verbose) write(outunit,*) "zscale: ", zscale
    if(verbose) write(outunit,*) "rho = "
    do jj = 1, gvec%nr
       do itr = 1,gvec%syms%ntrans
          call unfold(gvec%r(1,jj),gvec%syms%trans(1,1,itr),gvec%shift,gg)
          rho((jj-1)*gvec%syms%ntrans + itr) = fft_box%zbox(gg(1),gg(2),gg(3))
       enddo
    end do
    call zscal(gvec%nr*gvec%syms%ntrans,zscale,rho,1)
    do jj = 1, gvec%nr
       do itr = 1,gvec%syms%ntrans
          call unfold(gvec%r(1,jj),gvec%syms%trans(1,1,itr),gvec%shift,gg)
          if(verbose .and. abs(rho((jj-1)*gvec%syms%ntrans + itr))>1.0) &
              write(outunit,'(6i4,i8,f14.7)') & 
              gvec%r(1,jj),gvec%r(2,jj),gvec%r(3,jj),gg(1),gg(2),gg(3),(jj-1)*gvec%syms%ntrans+itr,rho((jj-1)*gvec%syms%ntrans + itr)
       enddo
    end do

  end subroutine zfullpoisson

  !===================================================================
  !
  ! Do an FFT in place: destroys contents of zbox
  ! and replaces them by the Fourier transform.
  !
  ! The FFT done is:
  !
  !   zbox(p) <- sum_j { zbox(j)*e^{sign*i*j.p} }
  !
  ! where j and p are integer 3 vectors ranging over Nfft(1:3).
  !
  ! MKL has a problem with argument overloading, so it does out-of-place
  ! FFT instead.
  !
  ! FFTW with real algebra has a funny data layout which requires a 4-th
  ! dimension ( nfft(4) ).
  !
  !-------------------------------------------------------------------
  subroutine zdo_FFT(sign,fft)

    use myconstants
    implicit none
    ! direction: sign = 1 does FFT from reciprocal space to real space
    !           sign = -1 does FFT from real space to reciprocal space
    integer, intent(in) :: sign
    ! FFT data
    type (fourier_transform), intent(inout) :: fft

    ! local variables

    if (sign /= 1 .and. sign /= -1) &
         call die('sign is not 1 or -1 in do_FFT')

    if (sign == 1) then
       call dfftw_execute( fft%plus_plan )
    else if (sign == -1) then
       call dfftw_execute( fft%minus_plan )
    endif

  end subroutine zdo_FFT
  !===================================================================
  !
  ! Initialize FFT environment. All arrays in structure "fft" are allocated
  ! here. Value of fft%trunc and fft%nfft are defined in parent routine.
  !
  !-------------------------------------------------------------------
  subroutine zinitialize_FFT(inode,fft)

    use myconstants
    implicit none

    ! arguments
    ! rank of current PE, for I/O only
    integer, intent(in) :: inode
    ! FFT data
    type (fourier_transform), intent(inout) :: fft

    ! local variables
    logical :: verbose
    integer :: info
    character (len=100) :: str

    verbose = .false.
    if (inode == 0) verbose = .true.
    fft%scale = one/((one*fft%nfft(1))*(one*fft%nfft(2))*(one*fft%nfft(3)))

    ! Allocate boxes: zbox holds densities, zcoul holds the Coulomb potential.
    fft%n_size = fft%nfft(1) * fft%nfft(2) * fft%nfft(3)
    allocate( fft%zbox(-fft%nfft(1)/2:fft%nfft(1)/2-1, &
                     -fft%nfft(2)/2:fft%nfft(2)/2-1, &
                     -fft%nfft(3)/2:fft%nfft(3)/2-1), stat=info)
    call alccheck('Zbox','intialize_FFT',fft%n_size,info)

    allocate( fft%zcoul(-fft%nfft(1)/2:fft%nfft(1)/2-1, &
                    -fft%nfft(2)/2:fft%nfft(2)/2-1, &
                    -fft%nfft(3)/2:fft%nfft(3)/2-1), stat=info)
    call alccheck('fft%Zcoul','initialize_FFT',fft%n_size,info)

    ! Initialize FFTW3 plans.
    write(str,'(a,3(i4,a))') ' Creating ', &
         fft%nfft(1),' x ',fft%nfft(2),' x ',fft%nfft(3),' FFTW 3 plans.'
    call stopwatch(verbose,str)
    call dfftw_plan_dft_3d(fft%plus_plan,fft%nfft(1),fft%nfft(2), &
         fft%nfft(3),fft%zbox,fft%zbox, fftw_backward, fftw_measure)
    call dfftw_plan_dft_3d(fft%minus_plan,fft%nfft(1),fft%nfft(2), &
         fft%nfft(3),fft%zbox,fft%zbox, fftw_forward, fftw_measure)

  end subroutine zinitialize_FFT
  !===================================================================
  !
  ! Undefine all FFT variables, including V_coul.
  !
  !-------------------------------------------------------------------
  subroutine zfinalize_FFT(inode,fft)

    use myconstants
    implicit none

    ! arguments
    ! rank of current PE, for I/O only
    integer, intent(in) :: inode
    ! FFT data
    type (fourier_transform), intent(inout) :: fft

    ! local variables
    logical :: verbose

    verbose = .false.
    if (inode == 0) verbose = .true.
    if (associated(fft%zcoul)) deallocate(fft%zcoul)
    if (associated(fft%zbox)) deallocate(fft%zbox)
    fft%scale = zero
    fft%n_size = 0
    call dfftw_destroy_plan(fft%minus_plan)
    call dfftw_destroy_plan(fft%plus_plan)
    call stopwatch(verbose,'FFT finalized')

  end subroutine zfinalize_FFT
  !===================================================================
  !
  ! Calculate Coulomb interaction,
  !        V_coul(G) = 4*pi*e^2/(q+G)^2 (rydberg units, e^2 = 2). 
  ! Use the same layout as the FFT boxes in subroutine "poisson".
  ! That saves reordering of arrays in the product zbox * zcoul.
  ! If the Coulomb potential is not truncated in real space
  ! (i.e., fft%trunc <= 0), then its singularity in reciprocal space
  ! must be removed. We do that by setting V_coul = 0 when |q+G| < qzero.
  ! Input vector q is given in units of reciprocal lattice vectors. This
  ! subroutine performs spherical truncation, so it is suitable for confined
  ! systems (with truncation) or bulk systems (without truncation). The
  ! truncation algorithm is presented in several articles, see for example:
  ! E. L. de la Grandmaison et al., Comput. Phys. Commun. 167, 7 (2005).
  !
  !-------------------------------------------------------------------
  subroutine zcreate_coul_0D(bdot,qq,fft)

    use myconstants
    implicit none

    ! arguments
    ! reciprocal space metric, q-vector in units of reciprocal lattice vectors
    real(dp), intent(in) :: bdot(3,3), qq(3)
    ! FFT data
    type (fourier_transform), intent(inout) :: fft

    ! local variables
    integer :: i1, i2, i3, j1, j2, j3
    real(dp) :: ekin, qpg(3)
    complex(dpc) :: rtmp

    fft%zcoul = zzero

    do j3 = -fft%nfft(3)/2, fft%nfft(3)/2 - 1
       qpg(3) = j3 + qq(3)
       i3 = j3 - fft%nfft(3)/2
       if (j3 < 0) i3 = fft%nfft(3) + i3
       do j2 = -fft%nfft(2)/2, fft%nfft(2)/2 - 1
          qpg(2) = j2 + qq(2)
          i2 = j2 - fft%nfft(2)/2
          if (j2 < 0) i2 = fft%nfft(2) + i2
          do j1 = -fft%nfft(1)/2, fft%nfft(1)/2 - 1
             qpg(1) = j1 + qq(1)
             i1 = j1 - fft%nfft(1)/2
             if (j1 < 0) i1 = fft%nfft(1) + i1

             ekin = dot_product(qpg,matmul(bdot,qpg))

             if (fft%trunc > zero) then
                if (ekin < qzero*qzero) then
                   rtmp = fft%trunc**2/two
                else
                   rtmp = (one-cos(fft%trunc*sqrt(ekin))) / ekin
                endif
             else
                if (ekin < qzero*qzero) then
                   rtmp = zzero
                else
                   rtmp = zone / ekin
                endif
             endif
             fft%zcoul(i1,i2,i3) = rtmp
          enddo
       enddo
    enddo

    rtmp = eight * pi
    call zscal(fft%n_size,rtmp,fft%zcoul,1)

  end subroutine zcreate_coul_0D
  !===================================================================
  !
  ! Calculate the Coulomb interaction with wire boundary conditions. The
  ! system is assumed to be periodic along x direction and confined along
  ! y and z directions. V_coul is calculated using truncation of periodic
  ! images following this method:
  ! S. Ismail-Beigi, Phys. Rev. B 73, 233103 (2006).
  !
  ! The Coulomb potential is calculated through 2-D FFT on the extended cell.
  !
  ! n_in is a (positive integer) parameter for real-space resolution.
  ! If the energy cutoff is too small, then the FFT grid may not be very
  ! dense and the Coulomb singularity at r = 0 may be poorly handled.
  ! Larger values of n_in will add extra points in between the original
  ! FFT grid. If the original grid has N points covering a space of length
  ! L, then the grid used to do 2-dimensional FFTs will have N * n_in points
  ! covering a space of length L.
  !
  ! When V_coul(G_1 + q,y,z) is calculated, we add a shift on the grid in
  ! order to avoid the singularity at y = z = 0.
  !
  ! Output is truncated zcoul(G_1+q,G_2,G_3) in reciprocal space such that,
  ! without truncation, its value would be:
  !
  !             zcoul(ig) = 8 Pi/(q + G)*2
  !
  ! for each G-vector in the FFT box.
  !
  ! Calculation is distributed over processors but array zcoul returns
  ! with global value.
  !
  !-------------------------------------------------------------------
  subroutine zcreate_coul_1D(inode,npes,comm,qq,gvec,fft)

    use typedefs
    use mpi_module
    implicit none

    ! arguments
    ! 1 data: PE rank, number of PEs, communicator
    integer, intent(in) :: inode, npes, comm
    ! q-vector, in units of reciprocal lattice vectors
    real(dp), intent(in) :: qq
    ! real-space grid
    type (gspace), intent(in):: gvec
    ! FFT data
    type (fourier_transform), intent(inout) :: fft

    ! local variables
    character (len=100) :: str
    logical :: verbose
    integer :: info, i1, i2, i3, j1, j2, j3, l2, l3, nfft_4, dNfft(3)
    real(dp) :: g1, gr(3), bdot(3,3), step(3)
!    real(dp) :: xdummy, bk0, bk1
    complex(dpc) :: zscale
    complex(dpc), pointer :: box_1D(:,:)

    ! FFTW plans, used by FFTW only
    integer (kind=8) :: minus_plan_1D

    verbose = .false.
    if (inode == 0) verbose = .true.

    ! Initialize two-dimensional FFT grid.
    dNfft(1) = fft%nfft(1)
    dNfft(2) = fft%nfft(2) * n_in * n_out
    dNfft(3) = fft%nfft(3) * n_in * n_out
    nfft_4 = dNfft(2)

    ! Initialize real-space metric in the yz plane. Assume that directions
    ! 2 and 3 are orthogonal (i.e. bdot(2,3) = bdot(3,2) = 0)
    step(1) = gvec%step(1)
    step(2) = gvec%step(2) / real( n_in, dp)
    step(3) = gvec%step(3) / real( n_in, dp)

    bdot = gvec%bdot
    bdot(2,2) = (two * pi / (step(2)*dNfft(2)))**2
    bdot(3,3) = (two * pi / (step(3)*dNfft(3)))**2

    ! Initialize boxes.
    allocate( box_1D(-dNfft(2)/2:dNfft(2)/2-1, &
                        -dNfft(3)/2:dNfft(3)/2-1), stat=info)
    i1 = dNfft(2) * dNfft(3)
    call alccheck('box_1D','create_coul_1D',i1,info)

    fft%zcoul = zzero
    if (verbose) then
       write(6,'(/,a)') ' Building truncated Coulomb potential using 2-D FFTs.'
       write(6,*) ' FFT grid = ', dNfft(2:3)
       write(6,*) ' Extra points between two points in 3-D FFT grid = ', n_in
       write(6,*) ' Number of y-z supercells = ', n_out
       write(6,*)
    endif

    ! Initialize FFTW3 plans.
    write(str,'(a,3(i4,a))') 'Creating ', &
         dNfft(2),' x',dNfft(3),' FFTW 3 plans '
    call stopwatch(verbose,str)
    call dfftw_plan_dft_2d(minus_plan_1D,dNfft(2), &
         dNfft(3),box_1D,box_1D, fftw_measure)
    !
    ! Start loop over planes of yz points.
    !
    do j1 = -fft%nfft(1)/2 + inode, fft%nfft(1)/2 - 1, npes
       g1 = abs(j1 + qq) * two * pi / gvec%avec(1,1)
       i1 = j1 - fft%nfft(1)/2
       if (j1 < 0) i1 = fft%nfft(1) + i1

       box_1D = zzero
       !
       ! For each plane, calculate the potential V_trunc(G_1+q,y,z).
       ! The potential is not zero only inside the Wigner-Seitz cell.
       ! Because of the logarithmic singularity, we must separate the case
       ! G_1 + q = 0.
       !
       if (g1 < qzero) then
          do j3 = -dNfft(3)/2/n_out, dNfft(3)/2/n_out - 1
             gr(3) = (j3 + gvec%shift(3)) * step(3)
             i3 = j3 - dNfft(3)/2
             if (j3 < 0) i3 = dNfft(3) + i3
             do j2 = -dNfft(2)/2/n_out, dNfft(2)/2/n_out - 1
                gr(2) = (j2 + gvec%shift(2)) * step(2)
                i2 = j2 - dNfft(2)/2
                if (j2 < 0) i2 = dNfft(2) + i2
                box_1D(i2,i3) = -log( sqrt(dot_product(gr(2:3),gr(2:3))) )
             enddo
          enddo
       else
          do j3 = -dNfft(3)/2/n_out, dNfft(3)/2/n_out - 1
             gr(3) = (j3 + gvec%shift(3)) * step(3)
             i3 = j3 - dNfft(3)/2
             if (j3 < 0) i3 = dNfft(3) + i3
             do j2 = -dNfft(2)/2/n_out, dNfft(2)/2/n_out - 1
                gr(2) = (j2 + gvec%shift(2)) * step(2)
                i2 = j2 - dNfft(2)/2
                if (j2 < 0) i2 = dNfft(2) + i2
!                xdummy = sqrt(dot_product(gr(2:3),gr(2:3))) * g1
!                call bessel_k(xdummy,bk0,bk1)
!                box_1D(i2,i3) = bk0
                box_1D(i2,i3) = &
                     bessel_k0( sqrt(dot_product(gr(2:3),gr(2:3))) * g1 )
             enddo
          enddo
       endif
       !
       ! Do a two-dimensional Fourier transform:
       !  V_trunc(G_1+q,y,z) -> V_trunc(G_1+q,G_2,G_3)
       !
       call dfftw_execute( minus_plan_1D )
       !
       ! Store the truncated potential V_trunc in zcoul.
       !
       do j3 = -fft%nfft(3)/2, fft%nfft(3)/2 - 1
          l3 = j3 - fft%nfft(3)/2
          if (j3 < 0) l3 = fft%nfft(3) + l3
          i3 = j3 * n_out - dNfft(3)/2 
          if (j3 < 0) i3 = dNfft(3) + i3
          do j2 = -fft%nfft(2)/2, fft%nfft(2)/2 - 1
             l2 = j2 - fft%nfft(2)/2
             if (j2 < 0) l2 = fft%nfft(2) + l2
             i2 = j2 * n_out - dNfft(2)/2
             if (j2 < 0) i2 = dNfft(2) + i2
             fft%zcoul(i1,l2,l3) = box_1D(i2,i3)
          enddo
       enddo
    enddo
    !
    ! Remove the long-wavelength (G1 = G2 = G3 = 0) part.
    !
    if (abs(qq) < qzero) then
       fft%zcoul(-fft%nfft(1)/2,-fft%nfft(2)/2,-fft%nfft(3)/2) = zzero
    endif
    !
    ! Global reduction if there is more than one processor. And rescale zcoul
    ! with the area of the unit cell in the yz plane.
    !
    call zpsum(fft%n_size,npes,comm,fft%zcoul)
    zscale = zone * four * step(2) * step(3)
    call zscal(fft%n_size,zscale,fft%zcoul,1)

    call dfftw_destroy_plan( minus_plan_1D )

    deallocate(box_1D)

  end subroutine zcreate_coul_1D
  !===================================================================
  !
  ! Calculate the Coulomb interaction with slab boundary conditions. The
  ! system is assumed to be periodic along x and y directions and confined
  ! along z direction. V_coul is calculated using truncation of periodic
  ! images following this method:
  ! S. Ismail-Beigi, Phys. Rev. B 73, 233103 (2006).
  !
  ! The Coulomb potential is calculated using Eq. 3 in the article above.
  !
  !-------------------------------------------------------------------
  subroutine zcreate_coul_2D(bdot,qq,fft)

    use typedefs
    implicit none

    ! arguments
    ! reciprocal space metric, q-vector in units of reciprocal lattice vectors
    real(dp), intent(in) :: bdot(3,3), qq(2)
    ! FFT data
    type (fourier_transform), intent(inout) :: fft

    ! local variables
    integer :: i1, i2, i3, j1, j2, j3, cosgz
    real(dp) :: gz, qpg2, qpg(2), bdot_2d(2,2)
    complex(dpc) :: rtmp

    fft%zcoul = zzero
    bdot_2d = bdot(1:2,1:2)

    do j3 = -fft%nfft(3)/2, fft%nfft(3)/2 - 1
       gz = j3 * pi / fft%trunc
       cosgz = 1 - 2 * mod(j3,2)
       i3 = j3 - fft%nfft(3)/2
       if (j3 < 0) i3 = fft%nfft(3) + i3
       do j2 = -fft%nfft(2)/2, fft%nfft(2)/2 - 1
          qpg(2) = j2 + qq(2)
          i2 = j2 - fft%nfft(2)/2
          if (j2 < 0) i2 = fft%nfft(2) + i2
          do j1 = -fft%nfft(1)/2, fft%nfft(1)/2 - 1
             qpg(1) = j1 + qq(1)
             i1 = j1 - fft%nfft(1)/2
             if (j1 < 0) i1 = fft%nfft(1) + i1

             qpg2 = dot_product(qpg,matmul(bdot_2d,qpg))
             if (qpg2 < qzero*qzero .and. j3 == 0) cycle
             rtmp = (one - exp(-sqrt(qpg2)*fft%trunc)*cosgz) / (qpg2 + gz*gz)

             fft%zcoul(i1,i2,i3) = rtmp
          enddo
       enddo
    enddo

    rtmp = eight * pi
    call zscal(fft%n_size,rtmp,fft%zcoul,1)

  end subroutine zcreate_coul_2D
  !===================================================================
  !
  ! Calculate the phase offset, bphase = exp( i qq.r ),
  ! where qq = k4 - k3 + k1 - k2.
  ! This is needed only in periodic systems. For confined systems
  ! (1 disabled) it returns bphase = one. Vector qq is assumed to
  ! be equal to some reciprocal lattice point.
  !
  !-------------------------------------------------------------------
  subroutine zget_phase(gvec,istart,nelem,qq,bphase)

    use myconstants
    implicit none

    ! arguments
    ! real-space grid
    type (gspace), intent(inout):: gvec
    ! address of first grid point in bphase, number of grid points in bphase
    integer, intent(in) :: istart, nelem
    ! q-vector, in units of reciprocal lattice vectors
    real(dp), intent(in) :: qq(3)
    ! phase, calculated only at grid points (istart+1) through (istart+nelem)
    complex(dpc), intent(out) :: bphase(nelem)

    ! local variables
    integer :: i1, i2, i3, j1, j2, j3, gg(3)
    real(dp) :: tsec(2)

    bphase = zone

    gg = nint(qq)
    !
    ! Compute phase in reciprocal space:
    !         bphase(G) = 1   if G = qq
    !         bphase(G) = 0   if G /= qq.
    !
    j3 = gg(3)
    i3 = j3 - fft_box%nfft(3)/2
    if (j3 < 0) i3 = fft_box%nfft(3) + i3
    j2 = gg(2)
    i2 = j2 - fft_box%nfft(2)/2
    if (j2 < 0) i2 = fft_box%nfft(2) + i2
    j1 = gg(1)
    i1 = j1 - fft_box%nfft(1)/2
    if (j1 < 0) i1 = fft_box%nfft(1) + i1

    fft_box%zbox = zzero
    fft_box%zbox(i1,i2,i3) = zone
    !
    ! Inverse Fourier transform: bphase(G) = FFT[ bphase(r) ] -> bphase(r)
    !
    call timacc(11,1,tsec)
    call zdo_FFT(1,fft_box)
    call timacc(11,2,tsec)
    !
    ! Reshape bphase(r) in the list of grid points.
    !
    i1 = 0
    do i2 = istart, istart + nelem - 1
       gg = gvec%r(:,i2)
       i1 = i1 + 1
       bphase(i1) = fft_box%zbox(gg(1),gg(2),gg(3))
    end do

  end subroutine zget_phase
  !===================================================================
  !
  ! Calculate the negative of the Laplacian using FFT.
  !
  ! INPUT:
  !   given_func : function f(r) on the real-space grid
  !   irp : irreducible representation of f(r), 1 for fully symmetric function.
  !
  ! OUTPUT:
  !   laplf : -Laplacian[ f(r) ] on the real-space grid
  !
  !-------------------------------------------------------------------
  subroutine zget_lap_FFT(gvec,given_func,laplf,irp)

    use typedefs
    implicit none

    ! arguments
    type (gspace), intent(inout):: gvec
    complex(dpc), intent(in) :: given_func(gvec%nr)
    complex(dpc), intent(out) :: laplf(gvec%nr)
    integer, intent(in) :: irp

    ! local variables
    integer :: i1, i2, i3, j1, j2, j3, jj, itr, gg(3)
    real(dp) :: tsec(2), charac(gvec%syms%ntrans), ekin
    complex(dpc) :: zscale

    !-------------------------------------------------------------------
    ! Put input function in fft box.
    !
    fft_box%zbox = zzero
    charac = real(gvec%syms%chi(irp,:),dp)
    do jj = 1, gvec%nr
       do itr = 1, gvec%syms%ntrans
          call unfold(gvec%r(1,jj),gvec%syms%trans(1,1,itr),gvec%shift,gg)
          fft_box%zbox(gg(1),gg(2),gg(3)) = given_func(jj) * charac(itr)
       enddo
    end do
    !
    ! Fourier transform: box(r) -> FFT[ box(r) ] = box(G).
    !
    call timacc(11,1,tsec)
    call zdo_FFT(-1,fft_box)
    call timacc(11,2,tsec)
    !
    ! Calculate Laplacian in reciprocal space: box(G) -> box(G) * G^2.
    !
    do j3 = -fft_box%nfft(3)/2, fft_box%nfft(3)/2 - 1
       gg(3) = j3
       i3 = j3 - fft_box%nfft(3)/2
       if (j3 < 0) i3 = fft_box%nfft(3) + i3
       do j2 = -fft_box%nfft(2)/2, fft_box%nfft(2)/2 - 1
          gg(2) = j2
          i2 = j2 - fft_box%nfft(2)/2
          if (j2 < 0) i2 = fft_box%nfft(2) + i2
          do j1 = -fft_box%nfft(1)/2, fft_box%nfft(1)/2 - 1
             gg(1) = j1
             i1 = j1 - fft_box%nfft(1)/2
             if (j1 < 0) i1 = fft_box%nfft(1) + i1
             ekin = dot_product(gg,matmul(gvec%bdot,gg))
             fft_box%zbox(i1,i2,i3) = fft_box%zbox(i1,i2,i3) * ekin
          enddo
       enddo
    enddo
    !
    ! Inverse Fourier transform: box(G) = FFT[ box(r) ] -> box(r).
    !
    call timacc(11,1,tsec)
    call zdo_FFT(1,fft_box)
    call timacc(11,2,tsec)
    !
    ! Store result in output array, laplf(r), and change its sign.
    !
    do jj = 1, gvec%nr
       gg = gvec%r(:,jj)
       laplf(jj) = fft_box%zbox(gg(1),gg(2),gg(3))
    end do
    zscale = -fft_box%scale
    call zscal(gvec%nr,zscale,laplf,1)

  end subroutine zget_lap_FFT
  !===================================================================
  !
  ! Calculate the gradient using FFT. Since a generic function and its
  ! gradient do not always belong to the same irreducible representation,
  ! the output gradient is calculated on the full grid, not only the
  ! irreducible wedge.
  !
  ! INPUT:
  !   given_func : function f(r) on the real-space grid
  !   irp : irreducible representation of f(r), 1 for fully symmetric function.
  !
  ! OUTPUT:
  !   gradf : Gradient[ f(r) ] on the real-space grid
  !
  !-------------------------------------------------------------------
  subroutine zget_grad_FFT(gvec,given_func,gradf,irp)

    use typedefs
    implicit none

    ! arguments
    type (gspace), intent(inout):: gvec
    complex(dpc), intent(in) :: given_func(gvec%nr)
    complex(dpc), intent(out) :: gradf(3,gvec%nr*gvec%syms%ntrans)
    integer, intent(in) :: irp

    ! local variables
    integer :: i1, i2, i3, j1, j2, j3, jj, itr, idir, gg(3)
    real(dp) :: tsec(2), charac(gvec%syms%ntrans), rr(3), rrp(3)
    complex(dpc) :: zscale

    do idir = 1, 3
       !-------------------------------------------------------------------
       ! Put input function in fft box.
       !
       fft_box%zbox = zzero
       charac = real(gvec%syms%chi(irp,:),dp)
       do jj = 1, gvec%nr
          do itr = 1, gvec%syms%ntrans
             call unfold(gvec%r(1,jj),gvec%syms%trans(1,1,itr),gvec%shift,gg)
             fft_box%zbox(gg(1),gg(2),gg(3)) = given_func(jj) * charac(itr)
          enddo
       end do
       !
       ! Fourier transform: box(r) -> FFT[ box(r) ] = box(G).
       !
       call timacc(11,1,tsec)
       call zdo_FFT(-1,fft_box)
       call timacc(11,2,tsec)
       !
       ! Calculate derivative in reciprocal space: box(G) -> box(G) * G.
       !
       do j3 = -fft_box%nfft(3)/2, fft_box%nfft(3)/2 - 1
          rr(3) = j3
          i3 = j3 - fft_box%nfft(3)/2
          if (j3 < 0) i3 = fft_box%nfft(3) + i3
          do j2 = -fft_box%nfft(2)/2, fft_box%nfft(2)/2 - 1
             rr(2) = j2
             i2 = j2 - fft_box%nfft(2)/2
             if (j2 < 0) i2 = fft_box%nfft(2) + i2
             do j1 = -fft_box%nfft(1)/2, fft_box%nfft(1)/2 - 1
                rr(1) = j1
                i1 = j1 - fft_box%nfft(1)/2
                if (j1 < 0) i1 = fft_box%nfft(1) + i1
                call dmatvec3('N',gvec%bvec,rr,rrp)
                fft_box%zbox(i1,i2,i3) = &
                     fft_box%zbox(i1,i2,i3) * rrp(idir)
             enddo
          enddo
       enddo
       !
       ! Inverse Fourier transform: box(G) = FFT[ box(r) ] -> box(r).
       !
       call timacc(11,1,tsec)
       call zdo_FFT(1,fft_box)
       call timacc(11,2,tsec)
       !
       ! Store result in output array, gradf(r).
       !
       charac = real(gvec%syms%chi(irp,:),dp)
       do jj = 1, gvec%nr
          do itr = 1, gvec%syms%ntrans
             call unfold(gvec%r(1,jj),gvec%syms%trans(1,1,itr),gvec%shift,gg)
             gradf(idir,jj + (itr-1)*gvec%nr) = &
                  fft_box%zbox(gg(1),gg(2),gg(3)) * charac(itr)
          enddo
       end do
    enddo

    zscale = fft_box%scale
    call zscal(3*gvec%nr*gvec%syms%ntrans,zscale,gradf,1)

  end subroutine zget_grad_FFT
  !===================================================================
  !
  ! Compute modified Bessel function K0(x) for x > 0.
  !
  ! Based on subroutine mik01a.for, copyrighted by Jianming Jin,
  ! S. Zhang and J. M. Jin, Computation of Special Functions.
  ! New York: John Wiley & Sons, 1996.
  ! http://jin.ece.uiuc.edu/routines/routines.html.
  !
  ! Simplified version by Murilo Tiago, tiagoml@ornl.gov, 2009
  !
  !-------------------------------------------------------------------
  function bessel_k0(xx) result(bk0)

    use myconstants
    implicit none

    real(dp), intent(in) :: xx
    real(dp) :: bk0
    real(dp) :: ct, cb, ca, rr, x2, xr, xr2, w0, ww, bi0
    integer :: kk, k0
    real(dp), parameter :: a1(8) = (/0.125d0, 0.2109375d0, &
         1.0986328125d0, 1.1775970458984d01, 2.1461706161499d02, &
         5.9511522710323d03, 2.3347645606175d05, 1.2312234987631d07/), &
         a2(12) = (/0.125d0, 7.03125d-2, 7.32421875d-2, &
         1.1215209960938d-1, 2.2710800170898d-1, 5.7250142097473d-1, &
         1.7277275025845d0, 6.0740420012735d0, 2.4380529699556d01, &
         1.1001714026925d02, 5.5133589612202d02, 3.0380905109224d03/)

    x2 = xx*xx
    if (xx == zero) then
       bk0 = 1.0d+300
       return
    else if (xx <= 18.0d0) then
       bi0 = one
       rr = one
       do kk = 1, 50
          rr = half*half*rr*x2/(kk*kk)
          bi0 = bi0 + rr
          if (dabs(rr/bi0) < 1.0d-15) exit
       enddo
    else
       k0 = 12
       if (xx >= 35.0) k0 = 9
       if (xx >= 50.0) k0 = 7
       ca = dexp(xx)/dsqrt(two*pi*xx)
       bi0 = one
       xr = one/xx
       do kk = 1, k0
          bi0 = bi0 + a2(kk)*xr**kk
       enddo
       bi0 = ca*bi0
    endif
    if (xx <= 9.0d0) then
       ct = -(dlog(xx*half)+euler_m)
       bk0 = zero
       w0 = zero
       ww = zero
       rr = one
       do kk = 1, 50
          w0 = w0 + 1.0d0/kk
          rr = half*half*rr/(kk*kk)*x2
          bk0 = bk0 + rr*(w0+ct)
          if (dabs((bk0-ww)/bk0) < 1.0d-15) exit
          ww = bk0
       enddo
       bk0 = bk0 + ct
    else
       cb = half/xx
       xr2 = one/x2
       bk0 = one
       do kk = 1, 8
          bk0 = bk0 + a1(kk)*xr2**kk
       enddo
       bk0 = cb*bk0/bi0
    endif
 
  end function bessel_k0
  !===================================================================
  !
  ! Define the average of the Coulomb potential inside a "mini"-Brillouin
  ! zone. This average is necessary because the Coulomb potential has
  ! a singularity at r=0. Averages below were calculated by averaging
  ! the Fourier transform of the potential (1/r) around the origin k=0.
  ! For simplicity, the vicinity of k=0 is taken to be circular
  ! (that is: spherical in 3d, cylindrical in 2d, straight line in 1d).
  !
  !-------------------------------------------------------------------
  function vq_average(per,nk,celvol,rmax) result(average)

    use myconstants
    implicit none

    ! arguments
    integer, intent(in) :: per, nk
    real(dp), intent(in) :: celvol, rmax
    real(dp) :: average

    select case(per)
    case(1)
       average = four * ( log(celvol*nk/rmax) + log(two) - euler_m ) / &
            (celvol*nk)
    case(2)
       average = four * sqrt(pi) / sqrt(celvol*nk)
    case(3)
       average = eight * exp(log(three/(four*pi*celvol*nk))/three)
    case default
       average = zero
    end select
    return

  end function vq_average
  !===================================================================
  !
  ! Increases the fft grid in nfft(3) to fit a value allowed by ESSL,
  ! e.g. a power of two, three etc.
  !
  ! Version 0: 1996 Bernd Pfrommer.
  ! symmetry-related features deleted by Murilo Tiago (2004).
  !
  !-------------------------------------------------------------------
  subroutine adjust_fft(nfft)

    use myconstants
    implicit none

    ! arguments
    integer, intent(inout) :: nfft(3)            ! fft grid dimensions


  end subroutine adjust_fft
!===============================================================
end module fft_module
